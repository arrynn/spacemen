﻿using System;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;

namespace Spacemen.BusinessLayer
{
    public interface IGameDriver
    {
        
        IGameConfig GetConfig();
        Task<Guid> GetMainTravelActivityId();
        Task<Guid> GetMainResearchActivityId();
        Task<Guid> GetMainSupervisionActivityId();
        Task<Guid> GetStartingPlanetId();
        void InvalidateStoredActivityTypes();
        void InvalidateStartingPlanet();
    }
}