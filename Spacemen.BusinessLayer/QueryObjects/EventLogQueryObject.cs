﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class EventLogQueryObject : QueryObjectBase<EventLogBaseDto, EventLog, EventLogFilterDto, IQuery<EventLog>>
    {
        public EventLogQueryObject(IMapper mapper, IQuery<EventLog> query) : base(mapper, query) { }

        protected override IQuery<EventLog> ApplyWhereClause(IQuery<EventLog> query, EventLogFilterDto filter)
        {
            if (filter.Id == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(EventLog.Id), ValueComparingOperator.Equal, filter.Id)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
