﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class DiscoveredPlanetQueryObject : QueryObjectBase<DiscoveredPlanetBaseDto, DiscoveredPlanet, DiscoveredPlanetFilterDto, IQuery<DiscoveredPlanet>>
    {
        public DiscoveredPlanetQueryObject(IMapper mapper, IQuery<DiscoveredPlanet> query) : base(mapper, query) { }

        protected override IQuery<DiscoveredPlanet> ApplyWhereClause(IQuery<DiscoveredPlanet> query, DiscoveredPlanetFilterDto filter)
        {
            if (filter.SpacemanId == Guid.Empty && filter.PlanetId == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(DiscoveredPlanet.SpacemanId), ValueComparingOperator.Equal, filter.SpacemanId),
                new SimplePredicate(nameof(DiscoveredPlanet.PlanetId), ValueComparingOperator.Equal, filter.PlanetId)
            };
            LogicalOperator op;
            if (filter.SpacemanId == Guid.Empty || filter.PlanetId == Guid.Empty)
            {
                op = LogicalOperator.OR;
            }
            else
            {
                op = LogicalOperator.AND;
            }
            
            var predicate = new CompositePredicate(list, op);
            return query.Where(predicate);
        }
    }
}
