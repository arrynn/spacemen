﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class SupervisedActivityQueryObject : QueryObjectBase<SupervisedActivityBaseDto, SupervisedActivity, SupervisedActivityFilterDto, IQuery<SupervisedActivity>>
    {
        public SupervisedActivityQueryObject(IMapper mapper, IQuery<SupervisedActivity> query) : base(mapper, query) { }

        protected override IQuery<SupervisedActivity> ApplyWhereClause(IQuery<SupervisedActivity> query, SupervisedActivityFilterDto filter)
        {
            if (filter.SupervisionId != Guid.Empty)
            {
                return query.Where(new SimplePredicate(nameof(SupervisedActivity.SupervisionId),
                    ValueComparingOperator.Equal, filter.SupervisionId));
            }

            if (filter.Id == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(SupervisedActivity.Id), ValueComparingOperator.Equal, filter.Id)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
