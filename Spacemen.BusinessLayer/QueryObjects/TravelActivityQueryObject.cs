﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class TravelActivityQueryObject : QueryObjectBase<TravelActivityBaseDto, TravelActivity, TravelActivityFilterDto, IQuery<TravelActivity>>
    {
        public TravelActivityQueryObject(IMapper mapper, IQuery<TravelActivity> query) : base(mapper, query) { }

        protected override IQuery<TravelActivity> ApplyWhereClause(IQuery<TravelActivity> query, TravelActivityFilterDto filter)
        {
            if (filter.Id == Guid.Empty && filter.ActivityId == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(TravelActivity.Id), ValueComparingOperator.Equal, filter.Id),
                new SimplePredicate(nameof(TravelActivity.ActivityId), ValueComparingOperator.Equal, filter.ActivityId)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
