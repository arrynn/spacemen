﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class SupervisionQueryObject : QueryObjectBase<SupervisionBaseDto, Supervision, SupervisionFilterDto, IQuery<Supervision>>
    {
        public SupervisionQueryObject(IMapper mapper, IQuery<Supervision> query) : base(mapper, query) { }

        protected override IQuery<Supervision> ApplyWhereClause(IQuery<Supervision> query, SupervisionFilterDto filter)
        {
            if (filter.Id == Guid.Empty && filter.ActivityId == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(Supervision.Id), ValueComparingOperator.Equal, filter.Id),
                new SimplePredicate(nameof(Supervision.ActivityId), ValueComparingOperator.Equal, filter.ActivityId)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
