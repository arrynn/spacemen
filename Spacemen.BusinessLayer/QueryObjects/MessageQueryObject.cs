﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class MessageQueryObject : QueryObjectBase<MessageBaseDto, Message, MessageFilterDto, IQuery<Message>>
    {
        public MessageQueryObject(IMapper mapper, IQuery<Message> query) : base(mapper, query) { }

        protected override IQuery<Message> ApplyWhereClause(IQuery<Message> query, MessageFilterDto filter)
        {
            if (filter.Id == Guid.Empty) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(Message.Id), ValueComparingOperator.Equal, filter.Id)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
