﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class PlanetQueryObject : QueryObjectBase<PlanetBaseDto, Planet, PlanetFilterDto, IQuery<Planet>>
    {
        public PlanetQueryObject(IMapper mapper, IQuery<Planet> query) : base(mapper, query) { }

        protected override IQuery<Planet> ApplyWhereClause(IQuery<Planet> query, PlanetFilterDto filter)
        {
            if (filter.Id == Guid.Empty && filter.Name == null) {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(Planet.Id), ValueComparingOperator.Equal, filter.Id),
                new SimplePredicate(nameof(Planet.Name), ValueComparingOperator.Equal, filter.Name)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
