﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class SpacemanQueryObject : QueryObjectBase<SpacemanBaseDto, Spaceman, SpacemanFilterDto, IQuery<Spaceman>>
    {
        public SpacemanQueryObject(IMapper mapper, IQuery<Spaceman> query) : base(mapper, query) { }

        protected override IQuery<Spaceman> ApplyWhereClause(IQuery<Spaceman> query, SpacemanFilterDto filter)
        {
            // listing spacemen on planet
            if (filter.PlanetId != Guid.Empty)
            {
                return query.Where(new SimplePredicate(nameof(Spaceman.PlanetId), ValueComparingOperator.Equal,
                    filter.PlanetId));
            }

            if (filter.Id == Guid.Empty && filter.Name == null)
            {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;
            }
            var list = new List<IPredicate>() {
                new SimplePredicate(nameof(Spaceman.Id), ValueComparingOperator.Equal, filter.Id),
                new SimplePredicate(nameof(Spaceman.Name), ValueComparingOperator.Equal, filter.Name)
            };
            var predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}
