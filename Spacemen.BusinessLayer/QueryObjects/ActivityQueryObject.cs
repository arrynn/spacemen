﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;
using Spacemen.Infrastructure.Query.Predicates;
using Spacemen.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;
using System.Linq;

namespace Spacemen.BusinessLayer.QueryObjects
{
    public class ActivityQueryObject : QueryObjectBase<ActivityBaseDto, Activity, ActivityFilterDto, IQuery<Activity>>
    {
        public ActivityQueryObject(IMapper mapper, IQuery<Activity> query) : base(mapper, query)
        {
        }

        protected override IQuery<Activity> ApplyWhereClause(IQuery<Activity> query, ActivityFilterDto filter)
        {
            List<IPredicate> list;
            CompositePredicate predicate;

            if (filter.ListBySpacemen)
            {
                var spacemenIdPredicates = new List<IPredicate>(filter.spacemenIds.Select(id =>
                    new SimplePredicate(nameof(Activity.SpacemanId), ValueComparingOperator.Equal, id)));
                var idsPredicate = new CompositePredicate(spacemenIdPredicates, LogicalOperator.OR);

                var conditions = new List<IPredicate>
                {
                    new SimplePredicate(nameof(Activity.ActivityTypeId), ValueComparingOperator.Equal,
                        filter.ActivityTypeId),
                    new SimplePredicate(nameof(Activity.Finished), ValueComparingOperator.Equal, filter.Finished),
                    new SimplePredicate(nameof(Activity.Cancelled), ValueComparingOperator.Equal, filter.Cancelled),
                };
                var conditionsPredicate = new CompositePredicate(conditions, LogicalOperator.AND);

                var bothPredicates = new List<IPredicate>();
                bothPredicates.Add(idsPredicate);
                bothPredicates.Add(conditionsPredicate);
                return Query.Where(new CompositePredicate(bothPredicates, LogicalOperator.AND));
            }

            if (filter.FilteringCompletion)
            {
                list = new List<IPredicate>()
                {
                    new SimplePredicate(nameof(Activity.End), ValueComparingOperator.LessThanOrEqual, filter.End),
                    new SimplePredicate(nameof(Activity.Cancelled), ValueComparingOperator.Equal, false),
                    new SimplePredicate(nameof(Activity.Finished), ValueComparingOperator.Equal, false),
                };
                predicate = new CompositePredicate(list);
                return query.Where(predicate);
            }

            if (filter.Id == Guid.Empty)
            {
                query.Where(new SimplePredicate(nameof(filter.Id), ValueComparingOperator.NotEqual, Guid.Empty));
                return query;            }
            list = new List<IPredicate>()
            {
                new SimplePredicate(nameof(Activity.Id), ValueComparingOperator.Equal, filter.Id)
            };
            predicate = new CompositePredicate(list, LogicalOperator.OR);
            return query.Where(predicate);
        }
    }
}