﻿using System;

namespace Spacemen.BusinessLayer.Utils.Date
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}