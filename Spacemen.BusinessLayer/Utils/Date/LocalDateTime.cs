﻿using System;

namespace Spacemen.BusinessLayer.Utils.Date
{
    public class LocalDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}