﻿using System;
using CodenameGenerator;

namespace Spacemen.BusinessLayer.Utils.Helpers
{
    public class GameUtil : IGameUtil
    {
        
        public bool Roll(int probability)
        {
            var rnd = new Random().Next(100);
            return rnd < probability;
        }
        
        public string GenerateRandomName()
        {
            return (new Generator()).Generate();
        }
    }
}