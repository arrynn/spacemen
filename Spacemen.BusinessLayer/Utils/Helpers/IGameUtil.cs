﻿namespace Spacemen.BusinessLayer.Utils.Helpers
{
    public interface IGameUtil
    {
        bool Roll(int probability);
        string GenerateRandomName();
    }
}