﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodenameGenerator;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.ActivityTypes;
using Spacemen.BusinessLayer.Services.Planets;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class PlanetFacade : FacadeBase
    {
        public IPlanetService PlanetService { get; set; }
        public IGameDriver GDriver { get; set; }

        public PlanetFacade(IUnitOfWorkProvider unitOfWorkProvider) : base(unitOfWorkProvider)
        {
        }

        public async Task<PlanetBaseDto> Get(Guid id)
        {
            using (Provider.Create())
            {
                var spaceman = await PlanetService.GetByIdAsync(id);
                return spaceman;
            }
        }

        public async Task<PlanetBaseDto> Get(string name)
        {
            using (Provider.Create())
            {
                var dto = await PlanetService.GetByNameAsync(name);
                return dto;
            }
        }

        public async Task<Guid> Create(PlanetBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                dto.Id = PlanetService.Create(dto);
                await uow.Commit();
                return dto.Id;
            }
        }

        public async Task<bool> Update(PlanetBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                if (await PlanetService.GetByIdAsync(dto.Id) == null)
                {
                    return false;
                }
                await PlanetService.Update(dto);
                await uow.Commit();
                // invalidates game driver main instances
                GDriver.InvalidateStartingPlanet();
                return true;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            using (var uow = Provider.Create())
            {
                if (await PlanetService.GetByIdAsync(id) == null)
                {
                    return false;
                }
                PlanetService.Delete(id);
                await uow.Commit();
                // invalidates game driver main instance
                GDriver.InvalidateStartingPlanet();
                return true;
            }
        }

        public async Task<IEnumerable<PlanetBaseDto>> All()
        {
            using (Provider.Create())
            {
                var result = await PlanetService.ListAsync(new PlanetFilterDto());
                return result.Items;
            }
        }
    }
}