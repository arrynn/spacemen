﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.DiscoveredPlanets;
using Spacemen.BusinessLayer.Services.Planets;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Services.TravelActivities;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class TravelFacade : FacadeBase, ITravelActivityPerformingFacade
    {
        public ISpacemanService SpacemanService { get; set; }
        public IActivityService ActivityService { get; set; }
        public IPlanetService PlanetService { get; set; }
        public PlanetFacade PlanetFacade { get; set; }
        public IDiscoveredPlanetService DiscoveredPlanetService { get; set; }
        public ITravelActivityService TravelActivityService { get; set; }
        public IDateTime LocalDateTime { get; set; }
        public IGameDriver GDriver { get; set; }

        public TravelFacade(IUnitOfWorkProvider unitOfWorkProvider) : base(unitOfWorkProvider)
        {
        }


        public async Task StartActivity(Guid spacemanId, Guid planetId)
        {
            using (var uow = Provider.Create())
            {
                var spaceman = await SpacemanService.GetByIdAsync(spacemanId);
                var activity = new ActivityBaseDto
                {
                    Start = LocalDateTime.Now,
                    End = LocalDateTime.Now.AddSeconds(GDriver.GetConfig().TimePerActivityInSeconds),
                    ActivityTypeId = await GDriver.GetMainTravelActivityId(),
//                    End = DateTime.Now.AddSeconds(
//                        //TODO: Refactor 
//                        GDriver.GetConfig().TimePerActivityInSeconds *
//                        (1 - ((GDriver.GetConfig().PercentageTravelSpeedIncreasePerRank * (spaceman.ExplorationRank )) /
//                              100))),
                    SpacemanId = spacemanId,
                };
                var newActivityId = ActivityService.Create(activity);

                var travelActivity = new TravelActivityBaseDto()
                {
                    ActivityId = newActivityId,
                    PlanetId = planetId
                };

                TravelActivityService.Create(travelActivity);

                await uow.Commit();
            }
        }

        public async Task FinishActivity(Guid activityId)
        {
            var activity = await ActivityService.GetByIdAsync(activityId);
            var spaceman = await SpacemanService.GetByIdAsync(activity.SpacemanId);
            var travelActivity = await TravelActivityService.GetByActivityIdAsync(activityId);

            activity.Finished = true;
            spaceman.PlanetId = travelActivity.PlanetId;
            spaceman.ExplorationRank += GDriver.GetConfig().RankIncreaseForTravel;
            spaceman.PlanetsVisited++;
            await ActivityService.Update(activity);
            await SpacemanService.Update(spaceman);
            var discoveredPlanet =
                await DiscoveredPlanetService.GetBySpacemanAndPlanetIdsAsync(spaceman.Id, travelActivity.PlanetId);
            discoveredPlanet.Visited = true;
            await DiscoveredPlanetService.Update(discoveredPlanet);
        }

        public async Task<IEnumerable<DiscoveredPlanetBaseDto>> AllDiscoveredPlanets()
        {
            using (var uow = Provider.Create())
            {
                return (await DiscoveredPlanetService.ListAllAsync()).Items;
            }
        }

        public async Task<IEnumerable<TravelActivityBaseDto>> All()
        {
            using (Provider.Create())
            {
                return (await TravelActivityService.ListAllAsync()).Items;
            }
        }

        public async Task<IEnumerable<DiscoveredPlanetBaseDto>> ListDiscoveredPlanetsBySpacemanId(Guid id)
        {
            using (Provider.Create())
            {
                var result = await DiscoveredPlanetService.ListAsync(new DiscoveredPlanetFilterDto
                {
                    SpacemanId = id,
                });
                return result.Items;
            }
        }
    }
}