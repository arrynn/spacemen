﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.Planets;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Services.SupervisedActivities;
using Spacemen.BusinessLayer.Services.Supervisions;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class SupervisionFacade : FacadeBase
    {
        public ISpacemanService SpacemanService { get; set; }
        public IPlanetService PlanetService { get; set; }
        public IActivityService ActivityService { get; set; }
        public SpacemanFacade SpacemanFacade { get; set; }
        public ActivityFacade ActivityFacade { get; set; }
        public ISupervisionService SupervisionService { get; set; }
        public ISupervisedActivityService SupervisedActivityService { get; set; }
        public ResearchFacade ResearchFacade { get; set; }
        public IDateTime LocalDateTime { get; set; }
        public IGameUtil GUtil { get; set; }
        public IGameDriver GDriver { get; set; }

        public SupervisionFacade(IUnitOfWorkProvider unitOfWorkProvider) : base(unitOfWorkProvider)
        {
        }

        public async Task StartActivity(Guid spacemanId)
        {
            using (var uow = Provider.Create())
            {
                var activities = await GetActivitiesBySpacemanId(spacemanId);
                var supervisionActivityId = await CreateSupervisionActivityBySpacemanId(spacemanId);
                var supervisionId = SupervisionService.Create(new SupervisionBaseDto
                {
                    SpacemanId = spacemanId,
                    ActivityId = supervisionActivityId
                });
                CreateSupervisedActivities(activities, spacemanId, supervisionId);
                await uow.Commit();
            }
        }

        private void CreateSupervisedActivities(IEnumerable<ActivityBaseDto> activities, Guid spacemanId,
            Guid supervisionId)
        {
            foreach (var activity in activities)
            {
                var supervisedActivity = new SupervisedActivityBaseDto
                {
                    SupervisionId = supervisionId,
                    ActivityId = activity.Id
                };

                SupervisedActivityService.Create(supervisedActivity);
            }
        }

        private async Task<IEnumerable<ActivityBaseDto>> GetActivitiesBySpacemanId(Guid spacemanId)
        {
            var spaceman = await SpacemanService.GetByIdAsync(spacemanId);
            var planetId = spaceman.PlanetId;
            var spacemen = await SpacemanFacade.ListByPlanetId(planetId);
            var pendingActivities = await ActivityFacade.ListPendingResearchActivitiesBySpacemen(spacemen);
            return pendingActivities;
        }

        private async Task<Guid> CreateSupervisionActivityBySpacemanId(Guid spacemanId)
        {
            var supervisionActivity = new ActivityBaseDto
            {
                Start = LocalDateTime.Now,
                End = LocalDateTime.Now.AddSeconds(GDriver.GetConfig().TimePerActivityInSeconds),
                ActivityTypeId = await GDriver.GetMainSupervisionActivityId(),
                SpacemanId = spacemanId,
            };

            return ActivityService.Create(supervisionActivity);
        }


        public async Task FinishActivity(Guid activityId)
        {
            var mainActivity = await ActivityService.GetByIdAsync(activityId);
            var supervisionBase = await SupervisionService.GetByActivityIdAsync(activityId);
            var supervision = await SupervisionService.GetByIdAsync(supervisionBase.Id);
            var spacemanId = supervision.SpacemanId;
            var spaceman = await SpacemanService.GetByIdAsync(spacemanId);
            var supervisedActivitiesQuery =
                await SupervisedActivityService.ListAsync(
                    new SupervisedActivityFilterDto {SupervisionId = supervision.Id});
            var supervisedActivities = supervisedActivitiesQuery.Items;
            var supervisionsDone = 0;
            foreach (var supervisedActivity in supervisedActivities)
            {
                var activity = await ActivityService.GetByIdAsync(supervisedActivity.ActivityId);
                if (activity.Finished
                    && GUtil.Roll(GDriver.GetConfig().PercentageProbabilityOfSupervisedActivityReward))
                {
                    await ResearchFacade.AssignRewardForResearch(supervision.SpacemanId);
                    supervisionsDone++;
                }
            }
            spaceman.SupervisionsCompleted += supervisionsDone;
            spaceman.ResearchRank += supervisionsDone * GDriver.GetConfig().RankIncreasePerSupervisedActivity;
            spaceman.InfluenceRank += GDriver.GetConfig().RankIncreaseForSupervision;
            mainActivity.Finished = true;
            await ActivityService.Update(mainActivity);
            await SpacemanService.Update(spaceman);
        }

        public async Task<IEnumerable<SupervisedActivityBaseDto>> AllSupervisedActivities()
        {
            using (Provider.Create())
            {
                return (await SupervisedActivityService.ListAllAsync()).Items;
            }
        }

        public async Task<IEnumerable<SupervisionBaseDto>> All()
        {
            using (Provider.Create())
            {
                return (await SupervisionService.ListAllAsync()).Items;
            }
        }
    }
}