﻿using System;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.DiscoveredPlanets;
using Spacemen.BusinessLayer.Services.Planets;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class ResearchFacade : FacadeBase, IActivityPerformingFacade
    {
        public ISpacemanService SpacemanService { get; set; }
        public IActivityService ActivityService { get; set; }
        public IPlanetService PlanetService { get; set; }
        public IDiscoveredPlanetService DiscoveredPlanetService { get; set; }
        public PlanetFacade PlanetFacade { get; set; }
        public IGameDriver GDriver { get; set; }
        public IGameUtil GUtil { get; set; }
        public IDateTime LocalDateTime { get; set; }

        public ResearchFacade(IUnitOfWorkProvider unitOfWorkProvider) : base(unitOfWorkProvider)
        {
        }

        public async Task StartActivity(Guid spacemanId)
        {
            using (var uow = Provider.Create())
            {
                var activity = new ActivityBaseDto
                {
                    Start = LocalDateTime.Now,
                    End = LocalDateTime.Now.AddSeconds(GDriver.GetConfig().TimePerActivityInSeconds),
                    ActivityTypeId = await GDriver.GetMainResearchActivityId(),
                    SpacemanId = spacemanId
                };

                ActivityService.Create(activity);
                await uow.Commit();
            }
        }

        public async Task FinishActivity(Guid activityId)
        {
            var activity = await ActivityService.GetByIdAsync(activityId);
            var spaceman = await SpacemanService.GetByIdAsync(activity.SpacemanId);

            if (GUtil.Roll(GDriver.GetConfig().PercentageProbabilityOfResearchReward))
            {
                await AssignRewardForResearch(spaceman.Id);
            }

            activity.Finished = true;
            await ActivityService.Update(activity);

            spaceman.ResearchRank += GDriver.GetConfig().RankIncreaseForResearch;
            spaceman.ResearchsCompleted++;
            await SpacemanService.Update(spaceman);

        }

        public async Task AssignRewardForResearch(Guid spacemanId)
        {
            var spacemansDiscoveredPlanets = await DiscoveredPlanetService.ListBySpacemanId(spacemanId);
            var allPlanets = await PlanetService.ListAllAsync();
            if (spacemansDiscoveredPlanets.TotalItemsCount < allPlanets.TotalItemsCount)
            {
                AssignRewardFromExistingPlanets(spacemanId, spacemansDiscoveredPlanets, allPlanets);
            }
            else
            {
                AssignRewardByCreatingNewPlanet(spacemanId);
            }
        }

        private void AssignRewardByCreatingNewPlanet(Guid spacemanId)
        {
            var generatedName = GUtil.GenerateRandomName();
            var newPlanetId =
                PlanetService.Create(new PlanetBaseDto {Name = generatedName, Description = "Newly found " + generatedName});
            var newDiscoveredPlanet = DiscoveredPlanetService.Create(new DiscoveredPlanetBaseDto
            {
                PlanetId = newPlanetId,
                SpacemanId = spacemanId
            });
        }

        private void AssignRewardFromExistingPlanets(Guid spacemanId,
            QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto> spacemansDiscoveredPlanets,
            QueryResultDto<PlanetBaseDto, PlanetFilterDto> allPlanets)
        {
            foreach (var currentPlanet in allPlanets.Items)
            {
                var isDiscovered = false;
                var lastPlanetId = currentPlanet.Id;
                foreach (var discoveredPlanet in spacemansDiscoveredPlanets.Items)
                {
                    if (discoveredPlanet.PlanetId == lastPlanetId)
                    {
                        isDiscovered = true;
                    }
                }
                if (!isDiscovered)
                {
                    var newDiscoveredPlanet = DiscoveredPlanetService.Create(new DiscoveredPlanetBaseDto
                    {
                        PlanetId = lastPlanetId,
                        SpacemanId = spacemanId
                    });
                }
            }
        }
    }
}