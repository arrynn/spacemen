﻿using System;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.Facades.Common
{
    public interface IActivityPerformingFacade
    {
        Task StartActivity(Guid spacemanId);
        Task FinishActivity(Guid activityId);
    }
}