﻿using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades.Common
{
    public abstract class FacadeBase
    {
        protected readonly IUnitOfWorkProvider Provider;

        protected FacadeBase(IUnitOfWorkProvider unitOfWorkProvider)
        {
            Provider = unitOfWorkProvider;
        }
    }
}