﻿using System;
using System.Threading.Tasks;

namespace Spacemen.BusinessLayer.Facades.Common
{
    public interface ITravelActivityPerformingFacade
    {
        Task StartActivity(Guid spacemanId, Guid planetId);
        Task FinishActivity(Guid activityId);
    }
}