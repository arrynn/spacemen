﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spacemen.BusinessLayer.Facades.Common;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;
using Spacemen.Infrastructure.UnitOfWork;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.DiscoveredPlanets;
using Spacemen.BusinessLayer.Services.Planets;

namespace Spacemen.BusinessLayer.Facades
{
    public class ActivityFacade : FacadeBase
    {
        
        public IActivityService ActivityService { get; set; }
        public IGameDriver GDriver { get; set; }
        
        public ActivityFacade(IUnitOfWorkProvider provider) : base(provider)
        {
        }

        public async Task<ActivityBaseDto> Get(Guid id)
        {
            using (Provider.Create())
            {
                var dto = await ActivityService.GetByIdAsync(id);
                return dto;
            }
        }

        public async Task<Guid> Create(ActivityBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                dto.Id = ActivityService.Create(dto);
                
                await uow.Commit();
                return dto.Id;
            }
        }

        public async Task<bool> Update(ActivityBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                if (await ActivityService.GetByIdAsync(dto.Id) == null)
                {
                    return false;
                }
                await ActivityService.Update(dto);
                await uow.Commit();
                return true;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            using (var uow = Provider.Create())
            {
                if (await ActivityService.GetByIdAsync(id) == null)
                {
                    return false;
                }
                ActivityService.Delete(id);
                await uow.Commit();
                return true;
            }
        }

        public async Task<IEnumerable<ActivityBaseDto>> All()
        {
            using (var uow = Provider.Create())
            {
                return (await ActivityService.ListAllAsync()).Items;
            }
        }

        public async Task<IEnumerable<ActivityBaseDto>> ListBySpacemanId(Guid id)
        {
            using (var uow = Provider.Create())
            {
                var result = await ActivityService.ListAsync(new ActivityFilterDto { SpacemanId = id});
                return result.Items;    
            }
        }

        public async Task<IEnumerable<ActivityBaseDto>> ListPendingResearchActivitiesBySpacemen(IEnumerable<SpacemanBaseDto> spacemen)
        {
            
            var researchTypeId = await GDriver.GetMainResearchActivityId();

            var spacemanIds = spacemen.Select(spaceman => spaceman.Id).ToList();

            var result = await ActivityService.ListAsync(new ActivityFilterDto
            {
                ListBySpacemen = true,
                ActivityTypeId = researchTypeId,
                Finished = false,
                Cancelled = false,
                spacemenIds = spacemanIds
                
            });
            return result.Items;
        }
    }
}