﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.Messages;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class MessageFacade : FacadeBase
    {
        public IMessageService MessageService { get; set; }

        public MessageFacade(IUnitOfWorkProvider unitOfWorkProvider) : base(unitOfWorkProvider)
        {
        }

        public async Task<MessageBaseDto> Get(Guid id)
        {
            using (Provider.Create())
            {
                var message = await MessageService.GetByIdAsync(id);
                return message;
            }
        }

        public async Task<Guid> Create(MessageBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                var id = MessageService.Create(dto);
                await uow.Commit();
                return id;
            }
        }

        public async Task<bool> Update(MessageBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                if (await MessageService.GetByIdAsync(dto.Id) == null)
                {
                    return false;
                }
                await MessageService.Update(dto);
                await uow.Commit();
                return true;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            using (var uow = Provider.Create())
            {
                if (await MessageService.GetByIdAsync(id) == null)
                {
                    return false;
                }
                MessageService.Delete(id);
                await uow.Commit();
                return true;
            }
        }

        public async Task<IEnumerable<MessageBaseDto>> All()
        {
            using (Provider.Create())
            {
                return (await MessageService.ListAllAsync()).Items;
            }
        }
    }
}