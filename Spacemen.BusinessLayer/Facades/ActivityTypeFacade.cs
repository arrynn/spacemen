﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spacemen.BusinessLayer.Facades.Common;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;
using Spacemen.Infrastructure.UnitOfWork;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.ActivityTypes;
using Spacemen.BusinessLayer.Services.DiscoveredPlanets;
using Spacemen.BusinessLayer.Services.Planets;

namespace Spacemen.BusinessLayer.Facades
{
    public class ActivityTypeFacade : FacadeBase
    {
        public IActivityTypeService ActivityTypeService { get; set; }
        public IGameDriver GDriver { get; set; }

        public ActivityTypeFacade(IUnitOfWorkProvider provider) : base(provider)
        {
        }

        public async Task<ActivityTypeBaseDto> Get(Guid id)
        {
            using (Provider.Create())
            {
                var spaceman = await ActivityTypeService.GetByIdAsync(id);
                return spaceman;
            }
        }

        public async Task<ActivityTypeBaseDto> Get(string name)
        {
            using (Provider.Create())
            {
                var dto = await ActivityTypeService.GetByNameAsync(name);
                return dto;
            }
        }

        public async Task<Guid> Create(ActivityTypeBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                dto.Id = ActivityTypeService.Create(dto);
                await uow.Commit();
                return dto.Id;
            }
        }

        public async Task<bool> Update(ActivityTypeBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                if (await ActivityTypeService.GetByIdAsync(dto.Id) == null)
                {
                    return false;
                }
                await ActivityTypeService.Update(dto);
                await uow.Commit();
                // invalidates game driver main instances
                GDriver.InvalidateStoredActivityTypes();
                return true;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            using (var uow = Provider.Create())
            {
                if (await ActivityTypeService.GetByIdAsync(id) == null)
                {
                    return false;
                }
                ActivityTypeService.Delete(id);
                await uow.Commit();
                // invalidates game driver main instances
                GDriver.InvalidateStoredActivityTypes();
                return true;
            }
        }

        public async Task<IEnumerable<ActivityTypeBaseDto>> All()
        {
            using (var uow = Provider.Create())
            {
                return (await ActivityTypeService.ListAllAsync()).Items;
            }
        }
    }
}