﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Facades
{
    public class GameFacade : FacadeBase
    {
        public SpacemanFacade SpacemanFacade { get; set; }
        public ISpacemanService SpacemanService { get; set; }
        public IActivityService ActivityService { get; set; }
        public IDateTime LocalDateTime { get; set; }

        public ResearchFacade ResearchFacade { get; set; }
        public SupervisionFacade SupervisionFacade { get; set; }
        public TravelFacade TravelFacade { get; set; }

        public IGameDriver GDriver { get; set; }
        
        public GameFacade(IUnitOfWorkProvider provider) : base(provider)
        {
        }


        public async Task Proceed()
        {
            using (var uow = Provider.Create())
            {
                var travelId = await GDriver.GetMainTravelActivityId();
                var researchId = await GDriver.GetMainResearchActivityId();
                var supervisionId = await GDriver.GetMainSupervisionActivityId();
                
                var overdueActivities = await GetOverdueActivities();
                foreach (var activity in overdueActivities)
                {
                    // TODO: refactor
                    if(activity.ActivityTypeId == travelId)
                        await TravelFacade.FinishActivity(activity.Id);
                    if(activity.ActivityTypeId == researchId)
                        await ResearchFacade.FinishActivity(activity.Id);
                    if(activity.ActivityTypeId == supervisionId)
                        await SupervisionFacade.FinishActivity(activity.Id);
                    await uow.Commit();
                }
            }
        }

        private async Task<IEnumerable<ActivityBaseDto>> GetOverdueActivities()
        {
            var activitiesQueryResult = await ActivityService.ListAsync(new ActivityFilterDto
            {
                FilteringCompletion = true,
                End = LocalDateTime.Now
            });
            return activitiesQueryResult.Items;
        }
    }
}