﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spacemen.BusinessLayer.Facades.Common;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.Config;
using Spacemen.Infrastructure.UnitOfWork;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.DiscoveredPlanets;
using Spacemen.BusinessLayer.Services.Planets;

namespace Spacemen.BusinessLayer.Facades
{
    public class SpacemanFacade : FacadeBase
    {
        public ISpacemanService SpacemanService { get; set; }
        public IPlanetService PlanetService { get; set; }
        public PlanetFacade PlanetFacade { get; set; }
        public IActivityService ActivityService { get; set; }
        public IDiscoveredPlanetService DiscoveredPlanetService { get; set; }
        public IGameDriver GDriver { get; set; }

        public SpacemanFacade(IUnitOfWorkProvider provider) : base(provider)
        {
        }

        public async Task<SpacemanBaseDto> Get(Guid id)
        {
            using (Provider.Create())
            {
                var dto = await SpacemanService.GetByIdAsync(id);
                return dto;
            }
        }

        public async Task<SpacemanBaseDto> Get(string name)
        {
            using (Provider.Create())
            {
                var dto = await SpacemanService.GetByNameAsync(name);
                return dto;
            }
        }

        public async Task<Guid> Create(SpacemanBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                await AppendPlanetToSpacemanIfNotExists(dto);
                dto.Id = SpacemanService.Create(dto);
                await AppendDiscoveredPlanetConnectionIfNotExists(dto);

                await uow.Commit();
                return dto.Id;
            }
        }

        private async Task AppendPlanetToSpacemanIfNotExists(SpacemanBaseDto dto)
        {
            if (dto.PlanetId == Guid.Empty)
            {
                dto.PlanetId = await GDriver.GetStartingPlanetId();
            }
        }

        private async Task AppendDiscoveredPlanetConnectionIfNotExists(SpacemanBaseDto dto)
        {
            var discoveredPlanet = await DiscoveredPlanetService.GetBySpacemanAndPlanetIdsAsync(dto.Id, dto.PlanetId);
            if (discoveredPlanet == null)
            {
                DiscoveredPlanetService.Create(
                    new DiscoveredPlanetBaseDto
                    {
                        SpacemanId = dto.Id,
                        PlanetId = dto.PlanetId
                    });
            }
        }

        public async Task<bool> Update(SpacemanBaseDto dto)
        {
            using (var uow = Provider.Create())
            {
                if (await SpacemanService.GetByIdAsync(dto.Id) == null)
                {
                    return false;
                }
                await SpacemanService.Update(dto);
                await uow.Commit();
                return true;
            }
        }

        public async Task<IEnumerable<SpacemanBaseDto>> All()
        {
            using (var uow = Provider.Create())
            {
                var result = await SpacemanService.ListAllAsync();
                return result.Items;
            }
        }

        public async Task<IEnumerable<SpacemanBaseDto>> ListByPlanetId(Guid planetId)
        {
            var result = await SpacemanService.ListAsync(new SpacemanFilterDto
            {
                PlanetId = planetId
            });
            return result.Items;
        }
    }
}