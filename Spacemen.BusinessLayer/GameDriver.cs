﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using CodenameGenerator;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.Facades;
using Spacemen.BusinessLayer.Services.ActivityTypes;

namespace Spacemen.BusinessLayer
{
    public class GameDriver : IGameDriver
    {
        private ActivityTypeBaseDto _mainTravelActivity;
        private ActivityTypeBaseDto _mainResearchActivity;
        private ActivityTypeBaseDto _mainSupervisionActivity;
        private IGameConfig _gameConfig;
        private PlanetBaseDto _startingPlanet;
        
        public ActivityTypeFacade ActivityTypeFacade { get; set; }
        public PlanetFacade PlanetFacade { get; set; }

        public GameDriver()
        {
        }

        public virtual IGameConfig GetConfig()
        {
            return _gameConfig ?? (_gameConfig = new GameConfig());
        }

        public async Task<Guid> GetMainTravelActivityId()
        {
            if (_mainTravelActivity == null)
            {
                _mainTravelActivity = await FindOrCreateActivityTypeByName(GetConfig().MainTravelActivityName);
            }
            return _mainTravelActivity.Id;
        }

        public async Task<Guid> GetMainResearchActivityId()
        {
            if (_mainResearchActivity == null)
            {
                _mainResearchActivity = await FindOrCreateActivityTypeByName(GetConfig().MainResearchActivityName);
            }
            return _mainResearchActivity.Id;
        }

        public async Task<Guid> GetMainSupervisionActivityId()
        {
            
            if (_mainSupervisionActivity == null)
            {
                _mainSupervisionActivity = await FindOrCreateActivityTypeByName(GetConfig().MainSupervisionActivityName);
            }
            return _mainSupervisionActivity.Id;
        }

        public async Task<Guid> GetStartingPlanetId()
        {
            if (_startingPlanet == null)
            {
                _startingPlanet = await FindOrCreatePlanetByName(GetConfig().StartingPlanetName);
            }
            return _startingPlanet.Id;
        }

        public void InvalidateStoredActivityTypes()
        {
            _mainTravelActivity = null;
            _mainResearchActivity = null;
            _mainSupervisionActivity = null;
        }

        public void InvalidateStartingPlanet()
        {
            _startingPlanet = null;
        }

        private async Task<ActivityTypeBaseDto> FindOrCreateActivityTypeByName(string name)
        {
            var activityType = await ActivityTypeFacade.Get(name);
            if (activityType != null) return activityType;

            activityType = new ActivityTypeBaseDto
            {
                Name = name,
                Description = $"Represents {name} activity type",
            };
            activityType.Id = await ActivityTypeFacade.Create(activityType);
            return activityType;
        }

        private async Task<PlanetBaseDto> FindOrCreatePlanetByName(string name)
        {
            var planet = await PlanetFacade.Get(name);
            if (planet != null) return planet;

            planet = new PlanetBaseDto
            {
                Name = name,
                Description = $"Represents the home planet {name}",
            };
            planet.Id = await PlanetFacade.Create(planet);
            return planet;
        }
    }
}