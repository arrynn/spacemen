﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class ActivityBaseDto : DtoBase
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool Finished { get; set; }
        public bool Cancelled { get; set; }
        
        public Guid ActivityTypeId { get; set; }
        public Guid SpacemanId { get; set; }

        public override string ToString() => $"Activity: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"Start[{Start.ToString()}] " +
                                             $"End[{End.ToString()}] " +
                                             $"Finished[{Finished}] " +
                                             $"Cancelled[{Cancelled}] " +
                                             $"ActivityTypeId[{ActivityTypeId}] " +
                                             $"SpacemanId[{SpacemanId}]";

    }
}
