﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SpacemanBaseDto : DtoBase
    {
        public string Name { get; set; }
        public int ExplorationRank { get; set; }
        public int ResearchRank { get; set; }
        public int InfluenceRank { get; set; }
        public int PlanetsVisited { get; set; }
        public int ResearchsCompleted { get; set; }
        public int SupervisionsCompleted { get; set; }

        public Guid PlanetId { get; set; }

        public SpacemanBaseDto()
        {
            ExplorationRank = 1;
            ResearchRank = 1;
            InfluenceRank = 1;

            PlanetsVisited = 1;
            ResearchsCompleted = 0;
            SupervisionsCompleted = 0;
        }

        public override string ToString() => $"Spaceman " +
                                             $"Name[{Name}] " +
                                             $"Id[{Id.ToString()}] " +
                                             $"PlanetId[{PlanetId.ToString()}] " +
                                             $"ExplorarionRank[{ExplorationRank}] " +
                                             $"ResearchRank[{ResearchRank}] " +
                                             $"InfluenceRank[{InfluenceRank}] " +
                                             $"PlanetsVisited[{PlanetsVisited}] " +
                                             $"ResearchsCompleted[{ResearchsCompleted}] " +
                                             $"SupervisionsCompleted[{SupervisionsCompleted}]";
    }
}