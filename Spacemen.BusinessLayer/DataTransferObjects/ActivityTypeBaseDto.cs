﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class ActivityTypeBaseDto : DtoBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public override string ToString() => $"ActivityType: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"Name[{Name}] " +
                                             $"Descrition[{Description}]";

    }
}