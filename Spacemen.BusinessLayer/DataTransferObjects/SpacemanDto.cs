﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SpacemanDto : SpacemanBaseDto
    {
        public PlanetDto Planet { get; set; }
        public ICollection<ActivityDto> Activities { get; set; }
        public ICollection<EventLogDto> EventLogs { get; set; }
        public ICollection<SupervisionDto> Supervisions{ get; set; }
    }
}