﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SupervisionDto : SupervisionBaseDto
    {
        public SpacemanDto Spaceman { get; set; }
        public ActivityDto Activity { get; set; }
        public ICollection<SupervisedActivityDto> SupervisedActivities { get; set; }
    }
}
