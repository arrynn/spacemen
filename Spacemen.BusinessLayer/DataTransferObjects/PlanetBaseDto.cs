﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class PlanetBaseDto : DtoBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public override string ToString() => $"Planet: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"Name[{Name}] " +
                                             $"Description[{Description}]";
    }
}