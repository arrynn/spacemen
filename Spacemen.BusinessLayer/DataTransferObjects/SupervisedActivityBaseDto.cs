﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SupervisedActivityBaseDto : DtoBase
    {
        public Guid SupervisionId { get; set; }

        public Guid ActivityId { get; set; }

        public override string ToString() => $"SupervisedActivity " +
                                             $"Id[{Id.ToString()}] " +
                                             $"SupervisionId[{SupervisionId.ToString()}] " +
                                             $"ActivityId[{ActivityId.ToString()}]";
    }
}
