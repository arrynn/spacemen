﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class EventLogBaseDto : DtoBase
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }

        public Guid SpacemanId { get; set; }

        public override string ToString() => $"EventLog: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"Date[{Date.ToString()}] " +
                                             $"Message[{Message}] " +
                                             $"SpacemanId[{SpacemanId.ToString()}]";
    }
}