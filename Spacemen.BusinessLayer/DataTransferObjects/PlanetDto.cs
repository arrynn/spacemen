﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class PlanetDto : PlanetBaseDto
    {
        public ICollection<TravelActivityDto> TravelActivities { get; set; }
        public ICollection<SpacemanDto> Spacemen { get; set; }
    }
}
