﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SupervisedActivityDto : SupervisedActivityBaseDto
    {
        public SupervisionDto Supervision { get; set; }
        public ActivityDto Activity { get; set; }
    }
}
