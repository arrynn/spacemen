﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class EventLogDto : EventLogBaseDto
    {
        public SpacemanDto Spaceman { get; set; }
    }
}