﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class TravelActivityBaseDto : DtoBase
    {
        public Guid ActivityId { get; set; }
        public Guid PlanetId { get; set; }

        public override string ToString() => $"TravelActivity " +
                                             $"Id[{Id.ToString()}] " +
                                             $"ActivityId[{ActivityId.ToString()}] " +
                                             $"PlanetId[{PlanetId.ToString()}]";
    }
}