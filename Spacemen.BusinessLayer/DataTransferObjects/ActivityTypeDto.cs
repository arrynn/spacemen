﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class ActivityTypeDto : ActivityTypeBaseDto
    {
        public ICollection<ActivityDto> Activities { get; set; }
    }
}