﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class SupervisionBaseDto : DtoBase
    {
        public Guid SpacemanId { get; set; }
        public Guid ActivityId { get; set; }

        public override string ToString() => $"Supervision" +
                                             $"Id[{Id.ToString()}] " +
                                             $"SpacemanId[{SpacemanId.ToString()}] " +
                                             $"ActivityId[{ActivityId.ToString()}]";
    }
}