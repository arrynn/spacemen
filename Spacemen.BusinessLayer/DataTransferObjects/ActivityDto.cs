﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;
using System.Collections.Generic;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class ActivityDto : ActivityBaseDto
    {
        public ActivityTypeDto ActivityType { get; set; }
        public SpacemanDto Spaceman { get; set; }
    }
}