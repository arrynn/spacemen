﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class MessageDto : MessageBaseDto
    {
        public SpacemanDto Sender { get; set; }
        public SpacemanDto Receiver { get; set; }
    }
}
