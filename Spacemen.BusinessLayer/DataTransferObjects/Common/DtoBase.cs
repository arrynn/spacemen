﻿using System;

namespace Spacemen.BusinessLayer.DataTransferObjects.Common
{
    public abstract class DtoBase
    {
        public Guid Id { get; set; }
    }
}
