﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class SupervisedActivityFilterDto : FilterDtoBase
    {
        public Guid SupervisionId { get; set; }
    }
}
