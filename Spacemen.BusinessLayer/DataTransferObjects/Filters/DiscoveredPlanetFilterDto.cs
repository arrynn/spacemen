﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class DiscoveredPlanetFilterDto : FilterDtoBase
    {
        public Guid SpacemanId { get; set; }
        public Guid PlanetId { get; set; }
    }
}
