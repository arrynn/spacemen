﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class SupervisionFilterDto : FilterDtoBase
    {
        public Guid ActivityId { get; set; }
    }
}
