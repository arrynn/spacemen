﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class TravelActivityFilterDto : FilterDtoBase
    {
        public Guid ActivityId { get; set; }
    }
}
