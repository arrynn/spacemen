﻿using Castle.MicroKernel.Registration;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class ActivityTypeFilterDto : FilterDtoBase
    {
        public string Name { get; set; }
    }
}
