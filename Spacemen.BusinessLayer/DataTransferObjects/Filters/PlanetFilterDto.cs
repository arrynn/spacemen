﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class PlanetFilterDto : FilterDtoBase
    {
        public string Name { get; set; }

    }
}
