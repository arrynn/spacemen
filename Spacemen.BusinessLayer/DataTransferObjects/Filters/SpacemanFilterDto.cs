﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class SpacemanFilterDto : FilterDtoBase
    {
        public string Name { get; set; }
        public Guid PlanetId { get; set; }
    }
}
