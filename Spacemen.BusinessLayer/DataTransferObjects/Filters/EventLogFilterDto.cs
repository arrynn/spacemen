﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class EventLogFilterDto : FilterDtoBase
    {
        public Guid SpacemanId { get; set; }
    }
}
