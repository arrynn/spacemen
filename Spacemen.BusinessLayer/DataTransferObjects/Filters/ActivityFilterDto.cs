﻿using System;
using System.Collections.Generic;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class ActivityFilterDto : FilterDtoBase
    {
        public Guid SpacemanId { get; set; }
        public bool FilteringCompletion { get; set; }
        public DateTime End { get; set; }

        public bool ListBySpacemen { get; set; }
        public Guid ActivityTypeId { get; set; }
        public bool Finished { get; set; }
        public bool Cancelled { get; set; }
        public List<Guid> spacemenIds { get; set; }
    }
}
