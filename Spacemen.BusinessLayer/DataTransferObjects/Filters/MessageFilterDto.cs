﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects.Filters
{
    public class MessageFilterDto : FilterDtoBase
    {
        public Guid SenderId { get; set; }
        public Guid ReceiverId { get; set; }
    }
}
