﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class TravelActivityDto : TravelActivityBaseDto
    {
        public ActivityDto Activity { get; set; }
        public PlanetDto Planet { get; set; }
    }
}