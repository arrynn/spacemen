﻿using Spacemen.BusinessLayer.DataTransferObjects.Common;
using System;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class MessageBaseDto : DtoBase
    {
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public bool Seen { get; set; }

        public int SenderId { get; set; }
        public int ReceiverId { get; set; }

        public override string ToString() => $"Message: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"Date[{Date.ToString()}] " +
                                             $"Text[{Text}] " +
                                             $"Seen[{Seen}] " +
                                             $"SenderId[{SenderId.ToString()}] " +
                                             $"ReceiverId[{ReceiverId.ToString()}]";
    }
}