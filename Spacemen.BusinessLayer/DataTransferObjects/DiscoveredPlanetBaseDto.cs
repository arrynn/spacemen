﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class DiscoveredPlanetBaseDto : DtoBase
    {
        public bool Visited { get; set; }
        public Guid PlanetId { get; set; }
        public Guid SpacemanId { get; set; }

        public DiscoveredPlanetBaseDto()
        {
            Visited = false;
        }

        public override string ToString() => $"DiscoveredPlanet: " +
                                             $"Id[{Id.ToString()}] " +
                                             $"PlanetId[{PlanetId.ToString()}] " +
                                             $"SpacemanId[{SpacemanId.ToString()}]";
    }
}