﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects.Common;

namespace Spacemen.BusinessLayer.DataTransferObjects
{
    public class DiscoveredPlanetDto : DiscoveredPlanetBaseDto
    {
        public PlanetDto Planet { get; set; }
        public SpacemanDto Spaceman { get; set; }
    }
}
