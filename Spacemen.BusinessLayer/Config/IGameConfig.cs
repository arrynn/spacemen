﻿namespace Spacemen.BusinessLayer.Config
{
    public interface IGameConfig
    {
        int RankIncreaseForTravel { get; }
        int PercentageTravelSpeedIncreasePerRank { get; }
        int RankIncreaseForResearch { get; }
        int RankIncreaseForSupervision { get; }
        int RankIncreasePerSupervisedActivity { get; }
        int PercentageProbabilityOfSupervisedActivityReward { get; }
        int PercentageProbabilityOfResearchReward { get; }
        int TimePerActivityInSeconds { get; }
        string StartingPlanetName { get; }
        string MainTravelActivityName { get; }
        string MainResearchActivityName { get; }
        string MainSupervisionActivityName { get; }
    }
}