﻿using System.Collections.Generic;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.DataAccessLayer.Entities;
using Spacemen.Infrastructure.Query;

namespace Spacemen.BusinessLayer.Config
{
    public class MappingConfig
    {
        public static void ConfigureMapping(IMapperConfigurationExpression config)
        {
            config.CreateMap<Activity, ActivityDto>();
            config.CreateMap<Activity, ActivityBaseDto>().ReverseMap();
            config.CreateMap<ActivityDto, ActivityBaseDto>().ReverseMap();
            
            config.CreateMap<ActivityType, ActivityTypeDto>();
            config.CreateMap<ActivityType, ActivityTypeBaseDto>().ReverseMap();
            config.CreateMap<ActivityTypeDto, ActivityTypeBaseDto>().ReverseMap();
           
            config.CreateMap<DiscoveredPlanet, DiscoveredPlanetDto>();
            config.CreateMap<DiscoveredPlanet, DiscoveredPlanetBaseDto>().ReverseMap();
            config.CreateMap<DiscoveredPlanetDto, DiscoveredPlanetBaseDto>().ReverseMap();
           
            config.CreateMap<EventLog, EventLogDto>();
            config.CreateMap<EventLog, EventLogBaseDto>().ReverseMap();
            config.CreateMap<EventLogDto, EventLogBaseDto>().ReverseMap();
            
            config.CreateMap<Message, MessageDto>();
            config.CreateMap<Message, MessageBaseDto>().ReverseMap();
            config.CreateMap<MessageDto, MessageBaseDto>().ReverseMap();
            
            config.CreateMap<Planet, PlanetDto>();
            config.CreateMap<Planet, PlanetBaseDto>().ReverseMap();
            config.CreateMap<PlanetDto, PlanetBaseDto>().ReverseMap();
            
            config.CreateMap<Spaceman, SpacemanDto>();
            config.CreateMap<Spaceman, SpacemanBaseDto>().ReverseMap();
            config.CreateMap<SpacemanDto, SpacemanBaseDto>().ReverseMap();

            config.CreateMap<SupervisedActivity, SupervisedActivityDto>();
            config.CreateMap<SupervisedActivity, SupervisedActivityBaseDto>().ReverseMap();
            config.CreateMap<SupervisedActivityDto, SupervisedActivityBaseDto>().ReverseMap();

            config.CreateMap<Supervision, SupervisionDto>();
            config.CreateMap<Supervision, SupervisionBaseDto>().ReverseMap();
            config.CreateMap<SupervisionDto, SupervisionBaseDto>().ReverseMap();

            config.CreateMap<TravelActivity, TravelActivityDto>();
            config.CreateMap<TravelActivity, TravelActivityBaseDto>().ReverseMap();
            config.CreateMap<TravelActivityDto, TravelActivityBaseDto>().ReverseMap();

            config.CreateMap<QueryResult<Activity>, QueryResultDto<ActivityBaseDto, ActivityFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<ActivityType>, QueryResultDto<ActivityTypeBaseDto, ActivityTypeFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<DiscoveredPlanet>, QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<EventLog>, QueryResultDto<EventLogBaseDto, EventLogFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<Message>, QueryResultDto<MessageBaseDto, MessageFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<Planet>, QueryResultDto<PlanetBaseDto, PlanetFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<Spaceman>, QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>>();
            config.CreateMap<QueryResult<SupervisedActivity>, QueryResultDto<SupervisedActivityBaseDto, SupervisedActivityFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<Supervision>, QueryResultDto<SupervisionBaseDto, SupervisionFilterDto>>().ReverseMap();
            config.CreateMap<QueryResult<TravelActivity>, QueryResultDto<TravelActivityBaseDto, TravelActivityFilterDto>>().ReverseMap();
        }
    }
}
