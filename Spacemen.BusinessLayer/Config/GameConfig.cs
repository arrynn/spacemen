﻿using System;
using CodenameGenerator;

namespace Spacemen.BusinessLayer.Config
{
 
    public class GameConfig : IGameConfig
    {
        public int RankIncreaseForTravel => 3;
        public int PercentageTravelSpeedIncreasePerRank => 1;
        public int RankIncreaseForResearch => 3;
        public int RankIncreaseForSupervision => 1;
        public int RankIncreasePerSupervisedActivity => 1;
        public int PercentageProbabilityOfSupervisedActivityReward => 25;
        public int PercentageProbabilityOfResearchReward => 50;
        public int TimePerActivityInSeconds => 2;

        public string StartingPlanetName => "Earth";
        public string MainTravelActivityName => "Travel";
        public string MainResearchActivityName => "Research";
        public string MainSupervisionActivityName => "Supervision";

    }
}