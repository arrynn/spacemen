﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Spacemen.DataAccessLayer.config;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using AutoMapper;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;

namespace Spacemen.BusinessLayer.Config
{
    public class BusinessLayerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            new EntityFrameworkInstaller().Install(container, store);

            container.Register(
                Classes.FromThisAssembly()
                    .BasedOn(typeof(QueryObjectBase<,,,>))
                    .WithServiceBase()
                    .LifestyleTransient(),
                Classes.FromThisAssembly()
                    .BasedOn<ServiceBase>()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient(),
                Classes.FromThisAssembly()
                    .BasedOn<FacadeBase>()
                    .LifestyleTransient(),
                Component.For<IMapper>()
                    .Instance(new Mapper(new MapperConfiguration(MappingConfig.ConfigureMapping)))
                    .LifestyleSingleton(),
                Component.For<IDateTime>()
                    .Instance(new LocalDateTime())
                    .LifestyleSingleton(),
                Classes.FromThisAssembly()
                    .BasedOn<IGameDriver>()
                    .WithServiceBase()
                    .LifestyleSingleton(),
                Component.For<IGameUtil>()
                    .Instance(new GameUtil())
                    .LifestyleSingleton()
            );
        }
    }
}