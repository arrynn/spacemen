﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.EventLogs
{
    public class EventLogService : CrudServiceBase<EventLog, EventLogDto, EventLogBaseDto, EventLogFilterDto>, IEventLogService
    {
        public EventLogService(IMapper mapper, IRepository<EventLog> repository,
            QueryObjectBase<EventLogBaseDto, EventLog, EventLogFilterDto, IQuery<EventLog>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<EventLog> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Spaceman"}
            return Repository.GetAsync(id);
        }
    }
}