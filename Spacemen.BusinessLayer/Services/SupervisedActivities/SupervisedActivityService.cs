﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.SupervisedActivities
{
    public class SupervisedActivityService : CrudServiceBase<SupervisedActivity, SupervisedActivityDto, SupervisedActivityBaseDto, SupervisedActivityFilterDto>, ISupervisedActivityService
    {
        public SupervisedActivityService(IMapper mapper, IRepository<SupervisedActivity> repository,
            QueryObjectBase<SupervisedActivityBaseDto, SupervisedActivity, SupervisedActivityFilterDto, IQuery<SupervisedActivity>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<SupervisedActivity> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Supervision", "Activity"}
            return Repository.GetAsync(id);
        }
    }
}