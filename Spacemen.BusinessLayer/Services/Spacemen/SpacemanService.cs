﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.Spacemen
{
    public class SpacemanService : CrudServiceBase<Spaceman, SpacemanDto, SpacemanBaseDto, SpacemanFilterDto>, ISpacemanService
    {
        public SpacemanService(IMapper mapper, IRepository<Spaceman> repository,
            QueryObjectBase<SpacemanBaseDto, Spaceman, SpacemanFilterDto, IQuery<Spaceman>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<Spaceman> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Activities", "Planet", "Supervisions", "EventLogs"}
            return Repository.GetAsync(id);
        }

        public async Task<SpacemanBaseDto> GetByNameAsync(string name)
        {
            var result = await Query.ExecuteQuery(new SpacemanFilterDto { Name = name});
            return result.Items.SingleOrDefault();
        }

    }
}