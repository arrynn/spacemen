﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using System.Threading.Tasks;

namespace Spacemen.BusinessLayer.Services.TravelActivities
{
    public interface ITravelActivityService
    {
        /// <summary>
        /// Gets entity with given id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entity with given id</returns>
        Task<TravelActivityBaseDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="id">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        Task<TravelActivityDto> GetAsync(Guid id, bool withIncludes = true);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Guid Create(TravelActivityBaseDto dto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Task Update(TravelActivityBaseDto dto);

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="id">Id of the entity to delete</param>
        void Delete(Guid id);
        
        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<TravelActivityBaseDto, TravelActivityFilterDto>> ListAllAsync();

        /// <summary>
        /// Gets all DTOs (for given type and filter)
        /// </summary>
        /// <param name="filter">Filter for DTOs</param>
        /// <returns>all DTOs passing the filter conditions</returns>
        Task<QueryResultDto<TravelActivityBaseDto, TravelActivityFilterDto>> ListAsync(TravelActivityFilterDto filter);

        /// <summary>
        /// Gets the travel activity corresponding to activity id
        /// </summary>
        /// <param name="activityId">activity id</param>
        /// <returns>travel activity corresponding to activity id</returns>
        Task<TravelActivityBaseDto> GetByActivityIdAsync(Guid activityId);
    }
}
