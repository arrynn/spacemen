﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.TravelActivities
{
    public class TravelActivityService : CrudServiceBase<TravelActivity, TravelActivityDto, TravelActivityBaseDto, TravelActivityFilterDto>, ITravelActivityService
    {
        public TravelActivityService(IMapper mapper, IRepository<TravelActivity> repository,
            QueryObjectBase<TravelActivityBaseDto, TravelActivity, TravelActivityFilterDto, IQuery<TravelActivity>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<TravelActivity> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Planet", "Activity"}
            return Repository.GetAsync(id);
        }

        public async Task<TravelActivityBaseDto> GetByActivityIdAsync(Guid activityId)
        {
            var result = await Query.ExecuteQuery(new TravelActivityFilterDto{ActivityId = activityId});
            return result.Items.FirstOrDefault();
        }
    }
}