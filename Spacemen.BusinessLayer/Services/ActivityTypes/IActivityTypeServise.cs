﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using System.Threading.Tasks;

namespace Spacemen.BusinessLayer.Services.ActivityTypes
{
    public interface IActivityTypeService
    {
        /// <summary>
        /// Gets entity with given id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entity with given id</returns>
        Task<ActivityTypeBaseDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Gets entity with given name
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>entity with given name</returns>
        Task<ActivityTypeBaseDto> GetByNameAsync(string name);

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="id">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        Task<ActivityTypeDto> GetAsync(Guid id, bool withIncludes = true);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Guid Create(ActivityTypeBaseDto dto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Task Update(ActivityTypeBaseDto dto);

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="id">Id of the entity to delete</param>
        void Delete(Guid id);
        
        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<ActivityTypeBaseDto, ActivityTypeFilterDto>> ListAllAsync();

        /// <summary>
        /// Gets all DTOs (for given type and filter)
        /// </summary>
        /// <param name="filter">Filter for DTOs</param>
        /// <returns>all DTOs passing the filter conditions</returns>
        Task<QueryResultDto<ActivityTypeBaseDto, ActivityTypeFilterDto>> ListAsync(ActivityTypeFilterDto filter);
    }
}
