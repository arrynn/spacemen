﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.ActivityTypes
{
    public class ActivityTypeService : CrudServiceBase<ActivityType, ActivityTypeDto, ActivityTypeBaseDto, ActivityTypeFilterDto>, IActivityTypeService
    {
        public ActivityTypeService(IMapper mapper, IRepository<ActivityType> repository,
            QueryObjectBase<ActivityTypeBaseDto, ActivityType, ActivityTypeFilterDto, IQuery<ActivityType>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<ActivityType> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Activities"}
            return Repository.GetAsync(id);
        }

        public async Task<ActivityTypeBaseDto> GetByNameAsync(string name)
        {
            var result = await Query.ExecuteQuery(new ActivityTypeFilterDto{ Name = name});
            return result.Items.SingleOrDefault();
        }
    }
}