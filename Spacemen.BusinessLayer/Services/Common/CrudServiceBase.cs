﻿using System;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using System.Linq;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects;

namespace Spacemen.BusinessLayer.Services.Common
{
    public abstract class CrudServiceBase<TEntity, TDto, TBaseDto, TFilterDto> : ServiceBase
        where TFilterDto : FilterDtoBase, new()
        where TEntity : class, IEntity, new()
        where TDto : DtoBase, TBaseDto
        where TBaseDto: DtoBase    
    {
        protected readonly IRepository<TEntity> Repository;

        protected readonly QueryObjectBase<TBaseDto, TEntity, TFilterDto, IQuery<TEntity>> Query;

        protected CrudServiceBase(IMapper mapper, IRepository<TEntity> repository,
            QueryObjectBase<TBaseDto, TEntity, TFilterDto, IQuery<TEntity>> query) : base(mapper)
        {
            this.Query = query;
            this.Repository = repository;
        }

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="id">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        public virtual async Task<TDto> GetAsync(Guid id, bool withIncludes = true)
        {
            TEntity entity;
            if (withIncludes)
            {
                entity = await GetWithIncludesAsync(id);
            }
            else
            {
                entity = await Repository.GetAsync(id);
            }
            return entity != null ? Mapper.Map<TDto>(entity) : null;
        }

        /// <summary>
        /// Gets entity (with complex types) according to ID
        /// </summary>
        /// <param name="id">entity ID</param>
        /// <returns>The DTO representing the entity</returns>
        protected abstract Task<TEntity> GetWithIncludesAsync(Guid id);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="dto">entity details</param>
        public virtual Guid Create(TBaseDto dto)
        {
            var baseDto = Mapper.Map<TBaseDto>(dto);
            var entity = Mapper.Map<TEntity>(baseDto);
            Repository.Create(entity);
            return entity.Id;
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="dto">entity details</param>
        public virtual async Task Update(TBaseDto dto)
        {
            var entity = await GetWithIncludesAsync(dto.Id);
            Mapper.Map(dto, entity);
            Repository.Update(entity);
        }

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="id">Id of the entity to delete</param>
        public virtual void Delete(Guid id)
        {
            Repository.Delete(id);
        }

        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        public virtual async Task<QueryResultDto<TBaseDto, TFilterDto>> ListAllAsync()
        {
            return await Query.ExecuteQuery(new TFilterDto());
        }

        public async Task<TBaseDto> GetByIdAsync(Guid id)
        {
            var result = await Query.ExecuteQuery(new TFilterDto {Id = id});
            return result.Items.SingleOrDefault();
        }

        public async Task<QueryResultDto<TBaseDto, TFilterDto>> ListAsync(TFilterDto filter)
        {
            return await Query.ExecuteQuery(filter);
        }
    }
}