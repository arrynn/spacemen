﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.DiscoveredPlanets
{
    public class DiscoveredPlanetService :
        CrudServiceBase<DiscoveredPlanet, DiscoveredPlanetDto, DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>, IDiscoveredPlanetService
    {
        public DiscoveredPlanetService(IMapper mapper, IRepository<DiscoveredPlanet> repository,
            QueryObjectBase<DiscoveredPlanetBaseDto, DiscoveredPlanet, DiscoveredPlanetFilterDto, IQuery<DiscoveredPlanet>>
                queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<DiscoveredPlanet> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Planet", "Spaceman"}
            return Repository.GetAsync(id);
        }

        public async Task<DiscoveredPlanetBaseDto> GetBySpacemanAndPlanetIdsAsync(Guid spacemanId, Guid planetId)
        {
            var result =
                await Query.ExecuteQuery(new DiscoveredPlanetFilterDto {SpacemanId = spacemanId, PlanetId = planetId});
            return result.Items.SingleOrDefault();
        }

        public async Task<QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>> ListBySpacemanId(
            Guid spacemanId)
        {
            return await Query.ExecuteQuery(new DiscoveredPlanetFilterDto {SpacemanId = spacemanId});
        }
    }
}