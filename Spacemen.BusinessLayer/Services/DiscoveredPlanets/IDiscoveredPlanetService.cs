﻿using System;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using System.Threading.Tasks;

namespace Spacemen.BusinessLayer.Services.DiscoveredPlanets
{
    public interface IDiscoveredPlanetService
    {
        /// <summary>
        /// Gets entity with given id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entity with given id</returns>
        Task<DiscoveredPlanetBaseDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="id">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        Task<DiscoveredPlanetDto> GetAsync(Guid id, bool withIncludes = true);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Guid Create(DiscoveredPlanetBaseDto dto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="dto">entity details</param>
        Task Update(DiscoveredPlanetBaseDto dto);

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="id">Id of the entity to delete</param>
        void Delete(Guid id);

        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>> ListAllAsync();

        /// <summary>
        /// Gets all DTOs (for given type and filter)
        /// </summary>
        /// <param name="filter">Filter for DTOs</param>
        /// <returns>all DTOs passing the filter conditions</returns>
        Task<QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>>
            ListAsync(DiscoveredPlanetFilterDto filter);

        /// <summary>
        /// Gets DTO representing the entity with given spaceman and planet
        /// </summary>
        /// <param name="spacemanId">spaceman ID</param>
        /// <param name="planetId">planet ID</param>
        /// <returns>The DTO representing the entity</returns>
        Task<DiscoveredPlanetBaseDto> GetBySpacemanAndPlanetIdsAsync(Guid spacemanId, Guid planetId);

        /// <summary>
        /// Gets result with spacemans discovered planets
        /// </summary>
        /// <param name="spacemanId">spaceman ID</param>
        /// <returns>The query result with entities</returns>
        Task<QueryResultDto<DiscoveredPlanetBaseDto, DiscoveredPlanetFilterDto>>
            ListBySpacemanId(Guid spacemanId);
    }
}