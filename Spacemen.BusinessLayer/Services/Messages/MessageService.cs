﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.Messages
{
    public class MessageService : CrudServiceBase<Message, MessageDto, MessageBaseDto, MessageFilterDto>, IMessageService
    {
        public MessageService(IMapper mapper, IRepository<Message> repository,
            QueryObjectBase<MessageBaseDto, Message, MessageFilterDto, IQuery<Message>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<Message> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Sender", "Receiver"}
            return Repository.GetAsync(id);
        }
    }
}