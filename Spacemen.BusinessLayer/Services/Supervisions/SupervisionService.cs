﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.Supervisions
{
    public class SupervisionService : CrudServiceBase<Supervision, SupervisionDto, SupervisionBaseDto, SupervisionFilterDto>, ISupervisionService
    {
        public SupervisionService(IMapper mapper, IRepository<Supervision> repository,
            QueryObjectBase<SupervisionBaseDto, Supervision, SupervisionFilterDto, IQuery<Supervision>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<Supervision> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Spaceman", "Activity", "SupervisedActivities"}
            return Repository.GetAsync(id);
        }

        public async Task<SupervisionBaseDto> GetByActivityIdAsync(Guid id)
        {
            var result = await Query.ExecuteQuery(new SupervisionFilterDto {ActivityId = id});
            return result.Items.FirstOrDefault();
        }
    }
}