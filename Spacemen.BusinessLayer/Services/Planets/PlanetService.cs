﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.Planets
{
    public class PlanetService : CrudServiceBase<Planet, PlanetDto, PlanetBaseDto, PlanetFilterDto>, IPlanetService
    {
        public PlanetService(IMapper mapper, IRepository<Planet> repository,
            QueryObjectBase<PlanetBaseDto, Planet, PlanetFilterDto, IQuery<Planet>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<Planet> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"Spacemen", "TravelActivities"}
            return Repository.GetAsync(id);
        }

        public async Task<PlanetBaseDto> GetByNameAsync(string name)
        { 
            var result = await Query.ExecuteQuery(new PlanetFilterDto{ Name = name});
            return result.Items.SingleOrDefault();
        }
    }
}