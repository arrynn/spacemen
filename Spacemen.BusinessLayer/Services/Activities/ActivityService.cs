﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.Query;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Services.Activities
{
    public class ActivityService : CrudServiceBase<Activity, ActivityDto, ActivityBaseDto, ActivityFilterDto>, IActivityService
    {
        public ActivityService(IMapper mapper, IRepository<Activity> repository,
            QueryObjectBase<ActivityBaseDto, Activity, ActivityFilterDto, IQuery<Activity>> queryObject)
            : base(mapper, repository, queryObject)
        {
        }

        protected override Task<Activity> GetWithIncludesAsync(Guid id)
        {
            //, new string[] {"ActivityType", "Spaceman", "SupervisedActivities", "TravelActivity"}
            return Repository.GetAsync(id);
        }

    }
}