﻿//using System;
//using System.Diagnostics;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Web.Http.Filters;
//using System.Web.Http.Controllers;
//using Castle.Core.Internal;
//using Spacemen.BusinessLayer.Facades;
//using Spacemen.WebApi.Controllers;
//
//namespace Spacemen.WebApi.Filters
//{
//    public class GameProceedFilter : ActionFilterAttribute
//    {
//        public override async Task OnActionExecutingAsync(HttpActionContext actionContext,
//            CancellationToken cancellationToken)
//        {
//            try
//            {
//                Console.WriteLine("Running the proceed method");
//                var controller = actionContext.ControllerContext.Controller;
//                var gameFacade = controller.GetType().GetProperty("GameFacade").GetValue(controller, null);
//                var typed = (GameFacade) gameFacade;
//                await typed.Proceed();
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine("Running the proceed method has failed\n" + ex.ToString());
//            }
//        }
//    }
//}