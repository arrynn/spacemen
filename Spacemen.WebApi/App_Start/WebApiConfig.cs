﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace Spacemen.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //config.Services.Add(typeof(GameProceedFilter), new GameProceedFilter());
            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html") );
            
            config.Routes.MapHttpRoute(
                "Default",
                "api/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = RouteParameter.Optional }
            );
        }
    }
}