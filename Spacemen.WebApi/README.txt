The context cannot be used while the model is being created. 
This exception may be thrown if the context is used inside the OnModelCreating method 
or if the same context instance is accessed by multiple threads concurrently.
Note that instance members of DbContext and related classes are not guaranteed to be thread safe.
