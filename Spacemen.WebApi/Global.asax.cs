﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Castle.Windsor;
using Spacemen.BusinessLayer.Config;

namespace Spacemen.WebApi
{
    public class Global : HttpApplication
    {
        private readonly IWindsorContainer container;

        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.Services
                .Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(container));
            
            //AreaRegistration.RegisterAllAreas();
            
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            
        }
        public Global()
        {
            this.container = new WindsorContainer().Install(new WebApiInstaller());
        }
 

        public override void Dispose()
        {
            container.Dispose();
            base.Dispose();
        }
    }
}