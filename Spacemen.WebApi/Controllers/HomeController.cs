﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Http;
using Spacemen.BusinessLayer.Facades;

namespace Spacemen.WebApi.Controllers
{
    public class HomeController : SpacemenApiController
    {
        
        [HttpGet]
        public string Index()
        {
            return "This is the API base for Spacemen project";
        }
    }
}