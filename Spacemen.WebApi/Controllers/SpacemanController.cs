﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Razor.Generator;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.Facades;

namespace Spacemen.WebApi.Controllers
{
    public class SpacemanController : SpacemenApiController
    {
        public SpacemanFacade SpacemanFacade { get; set; }

        public async Task Post([FromBody] SpacemanDto dto)
        {
        }

        [HttpGet]
        public async Task<SpacemanBaseDto> Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var dto = await SpacemanFacade.Get(name);
            if (dto == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return dto;
        }
    }
}