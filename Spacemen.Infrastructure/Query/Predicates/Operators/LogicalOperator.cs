﻿namespace Spacemen.Infrastructure.Query.Predicates.Operators
{
    public enum LogicalOperator
    {
        AND, OR
    }
}
