﻿using System;

namespace Spacemen.Infrastructure
{
    public interface IEntity
    {

        /// <summary>
        /// Name of the database table for this entity.
        /// </summary>
        string TableName { get; }

        /// <summary>
        /// Unique id of the entity.
        /// </summary>
        Guid Id { get; set; }
        
    }
}
