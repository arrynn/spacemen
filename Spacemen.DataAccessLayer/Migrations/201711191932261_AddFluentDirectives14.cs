namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFluentDirectives14 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TravelActivities");
            AddPrimaryKey("dbo.TravelActivities", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TravelActivities");
            AddPrimaryKey("dbo.TravelActivities", "ActivityId");
        }
    }
}
