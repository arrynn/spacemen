namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscoveredPlanetsEntityUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DiscoveredPlanets", "Visited", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DiscoveredPlanets", "Visited");
        }
    }
}
