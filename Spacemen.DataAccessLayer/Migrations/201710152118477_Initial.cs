namespace Spacemen.DataAccessLayer.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Start = c.DateTime(nullable: false),
                    End = c.DateTime(nullable: false),
                    Finished = c.Boolean(nullable: false),
                    Cancelled = c.Boolean(nullable: false),
                    ActivityTypeId = c.Guid(nullable: false),
                    SpacemanId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActivityTypes", t => t.ActivityTypeId)
                .ForeignKey("dbo.Spacemen", t => t.SpacemanId)
                .Index(t => t.ActivityTypeId)
                .Index(t => t.SpacemanId);

            CreateTable(
                "dbo.ActivityTypes",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    Description = c.String(nullable: false, maxLength: 250),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Spacemen",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    ExplorationRank = c.Int(nullable: false),
                    ResearchRank = c.Int(nullable: false),
                    InfluenceRank = c.Int(nullable: false),
                    PlanetsVisited = c.Int(nullable: false),
                    ResearchsCompleted = c.Int(nullable: false),
                    SupervisionsCompleted = c.Int(nullable: false),
                    PlanetId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Planets", t => t.PlanetId)
                .Index(t => t.PlanetId);

            CreateTable(
                "dbo.Planets",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    Description = c.String(nullable: false, maxLength: 250),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.DiscoveredPlanets",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    PlanetId = c.Guid(nullable: false),
                    SpacemanId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Planets", t => t.PlanetId)
                .ForeignKey("dbo.Spacemen", t => t.SpacemanId)
                .Index(t => t.PlanetId)
                .Index(t => t.SpacemanId);

            CreateTable(
                "dbo.EventLogs",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    Message = c.String(nullable: false, maxLength: 500),
                    SpacemanId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Spacemen", t => t.SpacemanId)
                .Index(t => t.SpacemanId);

            CreateTable(
                "dbo.Messages",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    Text = c.String(nullable: false, maxLength: 500),
                    Seen = c.Boolean(nullable: false),
                    SenderId = c.Guid(nullable: false),
                    ReceiverId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Spacemen", t => t.ReceiverId)
                .ForeignKey("dbo.Spacemen", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.ReceiverId);

            CreateTable(
                "dbo.SupervisedActivities",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    SupervisionId = c.Guid(nullable: false),
                    ActivityId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Supervisions", t => t.SupervisionId)
                .Index(t => t.SupervisionId)
                .Index(t => t.ActivityId);

            CreateTable(
                "dbo.Supervisions",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    SpacemanId = c.Guid(nullable: false),
                    ActivityId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Spacemen", t => t.SpacemanId)
                .Index(t => t.SpacemanId)
                .Index(t => t.ActivityId);

            CreateTable(
                "dbo.TravelActivities",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ActivityId = c.Guid(nullable: false),
                    PlanetId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Planets", t => t.PlanetId)
                .Index(t => t.ActivityId)
                .Index(t => t.PlanetId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.TravelActivities", "PlanetId", "dbo.Planets");
            DropForeignKey("dbo.TravelActivities", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions");
            DropForeignKey("dbo.Supervisions", "SpacemanId", "dbo.Spacemen");
            DropForeignKey("dbo.Supervisions", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.SupervisedActivities", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Messages", "SenderId", "dbo.Spacemen");
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.Spacemen");
            DropForeignKey("dbo.EventLogs", "SpacemanId", "dbo.Spacemen");
            DropForeignKey("dbo.DiscoveredPlanets", "SpacemanId", "dbo.Spacemen");
            DropForeignKey("dbo.DiscoveredPlanets", "PlanetId", "dbo.Planets");
            DropForeignKey("dbo.Spacemen", "PlanetId", "dbo.Planets");
            DropForeignKey("dbo.Activities", "SpacemanId", "dbo.Spacemen");
            DropForeignKey("dbo.Activities", "ActivityTypeId", "dbo.ActivityTypes");
            DropIndex("dbo.TravelActivities", new[] { "PlanetId" });
            DropIndex("dbo.TravelActivities", new[] { "ActivityId" });
            DropIndex("dbo.Supervisions", new[] { "ActivityId" });
            DropIndex("dbo.Supervisions", new[] { "SpacemanId" });
            DropIndex("dbo.SupervisedActivities", new[] { "ActivityId" });
            DropIndex("dbo.SupervisedActivities", new[] { "SupervisionId" });
            DropIndex("dbo.Messages", new[] { "ReceiverId" });
            DropIndex("dbo.Messages", new[] { "SenderId" });
            DropIndex("dbo.EventLogs", new[] { "SpacemanId" });
            DropIndex("dbo.DiscoveredPlanets", new[] { "SpacemanId" });
            DropIndex("dbo.DiscoveredPlanets", new[] { "PlanetId" });
            DropIndex("dbo.Spacemen", new[] { "PlanetId" });
            DropIndex("dbo.Activities", new[] { "SpacemanId" });
            DropIndex("dbo.Activities", new[] { "ActivityTypeId" });
            DropTable("dbo.TravelActivities");
            DropTable("dbo.Supervisions");
            DropTable("dbo.SupervisedActivities");
            DropTable("dbo.Messages");
            DropTable("dbo.EventLogs");
            DropTable("dbo.DiscoveredPlanets");
            DropTable("dbo.Planets");
            DropTable("dbo.Spacemen");
            DropTable("dbo.ActivityTypes");
            DropTable("dbo.Activities");
        }
    }
}
