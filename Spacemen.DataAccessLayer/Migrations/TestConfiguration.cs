namespace Spacemen.DataAccessLayer.Migrations
{
    using Spacemen.DataAccessLayer.Entities;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class TestConfiguration : DbMigrationsConfiguration<SpacemenTestDbContext>
    {
        public TestConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Spacemen.DataAccessLayer.SpacemenTestDbContext context)
        {
            // Test database is empty - all data should be created in test suites
        }
    }
}
