namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInverseProperties : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions");
            DropPrimaryKey("dbo.TravelActivities");
            DropPrimaryKey("dbo.Supervisions");
            AddPrimaryKey("dbo.TravelActivities", "ActivityId");
            AddPrimaryKey("dbo.Supervisions", "ActivityId");
            AddForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions", "ActivityId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions");
            DropPrimaryKey("dbo.Supervisions");
            DropPrimaryKey("dbo.TravelActivities");
            AddPrimaryKey("dbo.Supervisions", "Id");
            AddPrimaryKey("dbo.TravelActivities", "Id");
            AddForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions", "Id");
        }
    }
}
