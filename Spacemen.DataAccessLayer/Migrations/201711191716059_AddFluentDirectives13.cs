namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFluentDirectives13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions");
            DropPrimaryKey("dbo.Supervisions");
            AddPrimaryKey("dbo.Supervisions", "Id");
            AddForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions");
            DropPrimaryKey("dbo.Supervisions");
            AddPrimaryKey("dbo.Supervisions", "ActivityId");
            AddForeignKey("dbo.SupervisedActivities", "SupervisionId", "dbo.Supervisions", "ActivityId");
        }
    }
}
