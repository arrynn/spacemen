namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFluentDirectives4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TravelActivities", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Supervisions", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.SupervisedActivities", "ActivityId", "dbo.Activities");
            DropPrimaryKey("dbo.Activities");
            AddPrimaryKey("dbo.Activities", "Id");
            AddForeignKey("dbo.TravelActivities", "ActivityId", "dbo.Activities", "Id");
            AddForeignKey("dbo.Supervisions", "ActivityId", "dbo.Activities", "Id");
            AddForeignKey("dbo.SupervisedActivities", "ActivityId", "dbo.Activities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupervisedActivities", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Supervisions", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.TravelActivities", "ActivityId", "dbo.Activities");
            DropPrimaryKey("dbo.Activities");
            AddPrimaryKey("dbo.Activities", "ActivityTypeId");
            AddForeignKey("dbo.SupervisedActivities", "ActivityId", "dbo.Activities", "ActivityTypeId");
            AddForeignKey("dbo.Supervisions", "ActivityId", "dbo.Activities", "ActivityTypeId");
            AddForeignKey("dbo.TravelActivities", "ActivityId", "dbo.Activities", "ActivityTypeId");
        }
    }
}
