namespace Spacemen.DataAccessLayer.Migrations
{
    using Spacemen.DataAccessLayer.Entities;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<SpacemenDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Spacemen.DataAccessLayer.SpacemenDbContext context)
        {
            var p1 = Guid.NewGuid();
            var p2 = Guid.NewGuid();
            
            context.Planets.AddOrUpdate(
                new Planet { Id = p1, Name = "Earth", Description = "Our Homeland" },
                new Planet { Id = p2, Name = "Mars", Description = "The first stop" }
                );
            context.SaveChanges();

            var s1 = Guid.NewGuid();
            var s2 = Guid.NewGuid();
            var s3 = Guid.NewGuid();
            context.Spacemen.AddOrUpdate(
                new Spaceman { Id = s1, Name = "Marco", ResearchRank = 2, SupervisionsCompleted = 1, InfluenceRank = 3, PlanetId = p1 },
                new Spaceman { Id = s2, Name = "Andy", ResearchRank = 3, ResearchsCompleted = 1, PlanetId = p1 },
                new Spaceman { Id = s3,  Name = "Arnold", ExplorationRank = 3, PlanetsVisited = 1, ResearchsCompleted = 1, ResearchRank = 3, PlanetId = p2 }
                );
            context.SaveChanges();

            var at1 = Guid.NewGuid();
            var at2 = Guid.NewGuid();
            var at3 = Guid.NewGuid();
            context.ActivityTypes.AddOrUpdate(
                new ActivityType { Id = at1, Name = "Travel", Description = "This activity represents traveling between planets" },
                new ActivityType { Id = at2, Name = "Research", Description = "This activity represents researching - researching provides spacemen with technology to discover new planets" },
                new ActivityType { Id = at3, Name = "Supervision", Description = "This activity represents supervision of operations on the current planet. Supervision increases influence and has a chance to reward knowledge of new planets" }
                );
            context.SaveChanges();

            var a1 = Guid.NewGuid();
            var a2 = Guid.NewGuid();
            var a3 = Guid.NewGuid();
            var a4 = Guid.NewGuid();
            context.Activities.AddOrUpdate(
                // Spaceman arnold finished research with new planet reward
                new Activity { Id = a1, Start = new DateTime(2017, 10, 11, 12, 00, 00), End = new DateTime(2017, 10, 11, 14, 00, 00), ActivityTypeId = at2, SpacemanId = s3, Finished = true },
                // Arnold flies on the new planet Mars
                new Activity { Id = a2, Start = new DateTime(2017, 10, 11, 14, 00, 00), End = new DateTime(2017, 10, 11, 16, 00, 00), ActivityTypeId = at1, SpacemanId = s3, Finished = true },
                // Andy also did research
                new Activity { Id = a3, Start = new DateTime(2017, 10, 11, 20, 00, 00), End = new DateTime(2017, 10, 11, 22, 00, 00), ActivityTypeId = at2, SpacemanId = s2, Finished = true },
                // While andy did research, Marco supervised the operation, granting him half of the rank increase of Andy, but knowledge of the new planed is still unknown for him
                new Activity { Id = a4, Start = new DateTime(2017, 10, 11, 20, 00, 00), End = new DateTime(2017, 10, 11, 23, 00, 00), ActivityTypeId = at3, SpacemanId = s1, Finished = true }
                );
            context.SaveChanges();

            var dp1 = Guid.NewGuid();
            var dp2 = Guid.NewGuid();
            var dp3 = Guid.NewGuid();
            var dp4 = Guid.NewGuid();
            var dp5 = Guid.NewGuid();
            context.DiscoveredPlanets.AddOrUpdate(
                // all spacemen know about earth obviously
                new DiscoveredPlanet { Id = dp1, Visited = true, PlanetId = p1, SpacemanId = s1 },
                new DiscoveredPlanet { Id = dp2, Visited = true, PlanetId = p1, SpacemanId = s2 },
                new DiscoveredPlanet { Id = dp3, Visited = true, PlanetId = p1, SpacemanId = s3 },
                // andy and arnold know about Mars
                new DiscoveredPlanet { Id = dp4, Visited = false, PlanetId = p2, SpacemanId = s2 },
                new DiscoveredPlanet { Id = dp5, Visited = false,  PlanetId = p2, SpacemanId = s3 }
                );
            context.SaveChanges();

            var su1 = Guid.NewGuid();
            context.Supervisions.AddOrUpdate(
                // Marco has been supervising
                new Supervision { Id = su1, SpacemanId = s1, ActivityId = a4 }
                );
            context.SaveChanges();

            var sa1 = Guid.NewGuid();
            context.SupervisedActivities.AddOrUpdate(
                // Marco's supervision brings partial reward for Andy's research
                new SupervisedActivity { Id = sa1, ActivityId = a3, SupervisionId = su1 }
                );
            context.SaveChanges();

            var ta1 = Guid.NewGuid();
            context.TravelActivities.AddOrUpdate(
                // Arnold traveled to Mars
                new TravelActivity { Id = ta1, ActivityId = a2, PlanetId = p2 }
                );
            context.SaveChanges();

            var m1 = Guid.NewGuid();
            var m2 = Guid.NewGuid();
            context.Messages.AddOrUpdate(
                new Message { Id = m1, Date = new DateTime(2017, 10, 15, 23, 55, 00), SenderId = s1, ReceiverId = s2, Text = "Hello Andy, Don't forget to submit the Space project before deadline", Seen = true },
                new Message { Id = m2, Date = new DateTime(2017, 10, 15, 23, 59, 00), SenderId = s2, ReceiverId = s1, Text = "Hello Marco, Almost forgot, but i got it in time, thanks!" }
                );
            context.SaveChanges();

            var el1 = Guid.NewGuid();
            var el2 = Guid.NewGuid();
            var el3 = Guid.NewGuid();
            var el4 = Guid.NewGuid();
            var el5 = Guid.NewGuid();
            var el6 = Guid.NewGuid();
            var el7 = Guid.NewGuid();
            var el8 = Guid.NewGuid();
            context.EventLogs.AddOrUpdate(
                new EventLog { Id = el1, Date = new DateTime(2017, 10, 11, 12, 00, 00), SpacemanId = s3, Message = "Arnold started a research on planet Earth" },
                new EventLog { Id = el2, Date = new DateTime(2017, 10, 11, 14, 00, 00), SpacemanId = s3, Message = "Arnold has just finished his latest research. His Research rank was increased by 2. New planet travel possibility discovered: Mars." },
                new EventLog { Id = el3, Date = new DateTime(2017, 10, 11, 14, 00, 00), SpacemanId = s3, Message = "Arnold dispatched for Mars!" },
                new EventLog { Id = el4, Date = new DateTime(2017, 10, 11, 16, 00, 00), SpacemanId = s3, Message = "Arnold just arrived on Mars." },
                new EventLog { Id = el5, Date = new DateTime(2017, 10, 11, 20, 00, 00), SpacemanId = s2, Message = "Andy started a research on planet Earth" },
                new EventLog { Id = el6, Date = new DateTime(2017, 10, 11, 20, 00, 00), SpacemanId = s1, Message = "Marco started supervisioning operations on Earth" },
                new EventLog { Id = el7, Date = new DateTime(2017, 10, 11, 22, 00, 00), SpacemanId = s2, Message = "Andy has just finished his latest research. His Research rank was increased by 2. New planet travel possibility discovered: Mars." },
                new EventLog { Id = el8, Date = new DateTime(2017, 10, 11, 23, 00, 00), SpacemanId = s1, Message = "Marco finished his supervisioning. His Research rank was increased by 1 for the supervised operations." }
                );
            context.SaveChanges();
        }
    }
}
