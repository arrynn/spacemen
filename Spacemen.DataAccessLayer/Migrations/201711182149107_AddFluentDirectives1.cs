namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFluentDirectives1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.SupervisedActivities");
            AddPrimaryKey("dbo.SupervisedActivities", "SupervisionId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.SupervisedActivities");
            AddPrimaryKey("dbo.SupervisedActivities", "Id");
        }
    }
}
