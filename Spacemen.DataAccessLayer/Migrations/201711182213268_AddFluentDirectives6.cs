namespace Spacemen.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFluentDirectives6 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.SupervisedActivities");
            AddPrimaryKey("dbo.SupervisedActivities", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.SupervisedActivities");
            AddPrimaryKey("dbo.SupervisedActivities", "SupervisionId");
        }
    }
}
