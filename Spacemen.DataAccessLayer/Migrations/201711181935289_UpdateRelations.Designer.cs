// <auto-generated />
namespace Spacemen.DataAccessLayer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class UpdateRelations : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdateRelations));
        
        string IMigrationMetadata.Id
        {
            get { return "201711181935289_UpdateRelations"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
