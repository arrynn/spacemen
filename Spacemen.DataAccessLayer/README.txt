﻿Usage:

The app is configured for LocalDB. Actual migration schema is located in Migrations folder in file suffixed "_initial.cs".
Initial data for the project are located in seed method inside "Configuration.cs" file also located in Migrations folder.

1. Start a LocalDB instance at your machine as stated in the App.config OR rewrite the connection string to suit your localhost
2. Open Package Manager Console and run the command "Update-Database". This command applies the initial migration and runs the seed method from configuration.cs
3. Check your database via Server Explorer toolbar 
	3.1. (optional) Run "SqlLocalDb.exe info MSSQLLocalDB" in CMD to get instance pipe name of the LocalDB service 

Update:
1. Now you can migrate database by running the main method.

Update: 
1. For BusinessLayer tests, the SpacemenTestDbContext is used.

Author:
Marek Urban 422252@mail.muni.cz