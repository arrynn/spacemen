﻿using System;
using Spacemen.DataAccessLayer.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Spacemen.DataAccessLayer.Migrations;

namespace Spacemen.DataAccessLayer
{
    public class SpacemenTestDbContext : DbContext
    {
        //public SpacemenTestDbContext() : base(@"Server=(LocalDB)\MSSQLLocalDB;Database=SpacemenTest1;Trusted_Connection=True;MultipleActiveResultSets=True") {
        public SpacemenTestDbContext() : base(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;") {
            // force load of EntityFramework.SqlServer.dll into build
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            
            modelBuilder.Entity<ActivityType>()
                .HasMany(c => c.Activities)
                .WithRequired(c => c.ActivityType).HasForeignKey(c => c.ActivityTypeId);

            modelBuilder.Entity<Activity>()
                .HasRequired(c => c.Spaceman)
                .WithMany(s => s.Activities)
                .HasForeignKey(c => c.SpacemanId);

            modelBuilder.Entity<Spaceman>()
                .HasMany(c => c.Activities)
                .WithRequired(s => s.Spaceman)
                .HasForeignKey(k => k.SpacemanId);

            modelBuilder.Entity<SupervisedActivity>()
                .HasRequired(t => t.Supervision)
                .WithMany(t=>t.SupervisedActivities);

            modelBuilder.Entity<SupervisedActivity>()
                .HasRequired(t => t.Activity)
                .WithMany(t=>t.SupervisedActivities);

            modelBuilder.Entity<Supervision>()
                .HasRequired(t => t.Spaceman)
                .WithMany(t => t.Supervisions);
            
            modelBuilder.Entity<Supervision>()
                .HasRequired(t => t.Activity)
                .WithMany(t => t.Supervisions);

            modelBuilder.Entity<TravelActivity>()
                .HasRequired(t => t.Activity)
                .WithMany(t => t.TravelActivities)
                .HasForeignKey(t => t.ActivityId);
            
            modelBuilder.Entity<TravelActivity>()
                .HasRequired(t => t.Planet)
                .WithMany(t => t.TravelActivities)
                .HasForeignKey(t => t.PlanetId);
        }

        public void Nuke()
        {
            try
            {
                Console.WriteLine("Nuke() called. Killing the DB");
                Database.Delete();
                Database.Create();

                Database.SetInitializer(
                    new MigrateDatabaseToLatestVersion<SpacemenTestDbContext, TestConfiguration>()
                );
                Database.Initialize(false);
            }
            catch (Exception ex)
            {
                throw new Exception("error in SpacemenTestDbContext", ex);
            }
        }

        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<DiscoveredPlanet> DiscoveredPlanets { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Planet> Planets { get; set; }
        public DbSet<Spaceman> Spacemen { get; set; }
        public DbSet<SupervisedActivity> SupervisedActivities { get; set; }
        public DbSet<TravelActivity> TravelActivities { get; set; }
        public DbSet<Supervision> Supervisions { get; set; }
        public DbSet<EventLog> EventLogs { get; set; }
    }
}
