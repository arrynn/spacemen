﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class TravelActivity : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(TravelActivity);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid ActivityId { get; set; }
        [ForeignKey("ActivityId")]
        public virtual Activity Activity { get; set; }

        public Guid PlanetId { get; set; }
        [ForeignKey("PlanetId")]
        public virtual Planet Planet { get; set; }
    }
}
