﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;
using System.Collections.Generic;

namespace Spacemen.DataAccessLayer.Entities
{
    public class Supervision : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(Supervision);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid SpacemanId { get; set; }
        [ForeignKey("SpacemanId")]
        public virtual Spaceman Spaceman { get; set; }

        /// <summary>
        /// The activity ID which corresponds to this supervision
        /// </summary>
        [Required]
        public Guid ActivityId { get; set; }
        [ForeignKey("ActivityId")]
        public virtual Activity Activity { get; set; }

        public virtual ICollection<SupervisedActivity> SupervisedActivities { get; set; }

    }
}
