﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class EventLog : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(EventLog);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        [Required, MaxLength(500)]
        public string Message { get; set; }

        public Guid SpacemanId { get; set; }
        [ForeignKey("SpacemanId")]
        [InverseProperty("EventLogs")]
        public virtual Spaceman Spaceman { get; set; }
    }
}
