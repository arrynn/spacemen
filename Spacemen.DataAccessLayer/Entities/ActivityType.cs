﻿using System;
using Spacemen.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Spacemen.DataAccessLayer.Entities
{
    public class ActivityType : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(ActivityType);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required, MaxLength(250)]
        public string Description { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }

    }
}
