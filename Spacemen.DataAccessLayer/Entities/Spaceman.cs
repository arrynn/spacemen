﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class Spaceman : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(Spaceman);

        public Spaceman()
        {
            ExplorationRank = 1;
            ResearchRank = 1;
            InfluenceRank = 1;

            PlanetsVisited = 1;
            ResearchsCompleted = 0;
            SupervisionsCompleted = 0;
            
            Activities = new HashSet<Activity>();
            Planet = null;
            DiscoveredPlanets = new HashSet<DiscoveredPlanet>();
            EventLogs = new HashSet<EventLog>();
            Supervisions = new HashSet<Supervision>();
            SentMessages = new HashSet<Message>();
            ReceivedMessages = new HashSet<Message>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public int ExplorationRank { get; set; }

        public int ResearchRank { get; set; }

        public int InfluenceRank { get; set; }

        public int PlanetsVisited { get; set; }

        public int ResearchsCompleted { get; set; }

        public int SupervisionsCompleted { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }

        public Guid PlanetId { get; set; }
        [ForeignKey("PlanetId")]
        [InverseProperty("Spacemen")] 
        public virtual Planet Planet { get; set; }

        public virtual ICollection<DiscoveredPlanet> DiscoveredPlanets { get; set; }
        /// <summary>
        /// could not pass due to double-value referenced in Messages table
        /// </summary>
        //public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<EventLog> EventLogs { get; set; }

        public virtual ICollection<Supervision> Supervisions { get; set; }
        
        [InverseProperty("Sender")]
        public virtual ICollection<Message> SentMessages { get; set; }
       
        [InverseProperty("Receiver")]
        public virtual ICollection<Message> ReceivedMessages{ get; set; }
    }
}
