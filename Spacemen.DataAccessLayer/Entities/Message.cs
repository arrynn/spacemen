﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class Message : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(Message);

        public Message()
        {
            Seen = false;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        [Required, MaxLength(500)]
        public string Text { get; set; }

        public bool Seen { get; set; }

        public Guid SenderId { get; set; }
        [ForeignKey("SenderId")]
        [InverseProperty("SentMessages")]
        public virtual Spaceman Sender { get; set; }

        public Guid ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        [InverseProperty("ReceivedMessages")]
        public virtual Spaceman Receiver { get; set; }
    }
}
