﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace Spacemen.DataAccessLayer.Entities
{
    public class Planet : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(Planet);

        public Planet()
        {
            Description = "This planet has not been fully explored yet. Only a few individuals know of existence of this planet";
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required, MaxLength(250)]
        public string Description { get; set; }

        [InverseProperty("Planet")]
        public virtual ICollection<TravelActivity> TravelActivities { get; set; }

        public virtual ICollection<Spaceman> Spacemen { get; set; }

        public virtual ICollection<DiscoveredPlanet> DiscoveredPlanets { get; set; }


    }
}
