﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class DiscoveredPlanet : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(DiscoveredPlanet);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public bool Visited { get; set; }
        
        public Guid PlanetId { get; set; }
        [ForeignKey("PlanetId")]
        [InverseProperty("DiscoveredPlanets")]
        public virtual Planet Planet { get; set; }

        public Guid SpacemanId { get; set; }
        [ForeignKey("SpacemanId")]
        [InverseProperty("DiscoveredPlanets")]
        public virtual Spaceman Spaceman { get; set; }

    }
}
