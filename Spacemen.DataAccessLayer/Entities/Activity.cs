﻿using Spacemen.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace Spacemen.DataAccessLayer.Entities
{
    public class Activity : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(Activity);

        public Activity()
        {
            Finished = false;
            Cancelled = false;
            Spaceman = null;
            Supervisions = new HashSet<Supervision>();
            TravelActivities = new HashSet<TravelActivity>();
            SupervisedActivities = new HashSet<SupervisedActivity>();
            ActivityType = null;

        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public bool Finished { get; set; }

        public bool Cancelled { get; set; }

        public Guid ActivityTypeId { get; set; }
        [ForeignKey("ActivityTypeId")]
        public virtual ActivityType ActivityType { get; set; }

        public Guid SpacemanId { get; set; }
        [ForeignKey("SpacemanId")]
        public virtual Spaceman Spaceman { get; set; }

        public virtual ICollection<SupervisedActivity> SupervisedActivities{ get; set; }

        public virtual ICollection<Supervision> Supervisions { get; set; }

        public virtual ICollection<TravelActivity> TravelActivities { get; set; }

        
    }
}
