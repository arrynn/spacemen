﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Spacemen.Infrastructure;

namespace Spacemen.DataAccessLayer.Entities
{
    public class SupervisedActivity : IEntity
    {
        [NotMapped]
        public string TableName { get; } = nameof(SupervisedActivity);

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid SupervisionId { get; set; }
        [ForeignKey("SupervisionId")]
        public virtual Supervision Supervision { get; set; }

        public Guid ActivityId { get; set; }
        [ForeignKey("ActivityId")]
        public virtual Activity Activity { get; set; }
    }
}
