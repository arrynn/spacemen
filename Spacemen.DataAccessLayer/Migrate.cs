﻿using System;
using Spacemen.DataAccessLayer.Migrations;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Spacemen.DataAccessLayer
{
    class Migrate
    {
        public static void Main()
        {
            Console.WriteLine("Running database schema import:");
            Console.WriteLine("Select database - Test/Production (t/p):\n");
            var choice = Console.ReadLine();
            DbContext context;
            var isTestDb = false;
            if (choice.Equals("p"))
            {
                Console.WriteLine("Selected production database");
                isTestDb = true;
                context = new SpacemenDbContext();
            }
            else
            {
                Console.WriteLine("Selected test database");
                context = new SpacemenTestDbContext();
            }
            Console.WriteLine("WARNING! You are about to ERASE data in your database");
            Console.WriteLine("Do you want to replace data with seed defaults? y/n:\n");

             choice = Console.ReadLine();

            if (choice.Equals("y"))
            {   
                Console.WriteLine("\nProceeding...");

                

                Console.WriteLine("Deleting database");
                try
                {
                    context.Database.Delete();
                    Console.WriteLine("Done.\n");

                    Console.WriteLine("Creating database");
                    context.Database.Create();
                    Console.WriteLine("Done.\n");

                    Console.WriteLine("Initializing");
                    Console.WriteLine("Running Seed method");

                    if (isTestDb)
                    {
                        Database.SetInitializer(
                            new MigrateDatabaseToLatestVersion<SpacemenTestDbContext, TestConfiguration>()
                        );
                    }
                    else
                    {
                        Database.SetInitializer(
                            new MigrateDatabaseToLatestVersion<SpacemenDbContext, Configuration>()
                        );
                    }

                    context.Database.Initialize(false);

                    Console.WriteLine("Done.\n");

                    Console.WriteLine("Finished.\n");
                }
                catch (Exception ex) {
                    //Console.WriteLine("Database is in use. Please disconnect first and then try again.");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Abort.");
                }
            }
            else
            {
                Console.WriteLine("Abort.");
            }
        }
    }
}
