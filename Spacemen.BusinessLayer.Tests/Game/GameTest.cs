﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Castle.Core;
using Castle.Core.Internal;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ConsoleUtilities;
using Moq;
using NUnit.Framework;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.Facades;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Tests.Game.Common;
using Spacemen.BusinessLayer.Tests.Game.Config;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;
using Spacemen.DataAccessLayer;
using Spacemen.DataAccessLayer.Migrations;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Tests.Game
{
    [TestFixture]
    public class GameTest
    {
        #region Properties and Init

        private IWindsorContainer Container;

        private GameFacade GameFacade;
        private ResearchFacade ResearchFacade;
        private SupervisionFacade SupervisionFacade;
        private TravelFacade TravelFacade;
        private SpacemanFacade SpacemanFacade;
        private PlanetFacade PlanetFacade;
        private ActivityFacade ActivityFacade;
        private ActivityTypeFacade ActivityTypeFacade;
        private MessageFacade MessageFacade;

        private GameMockManager MockManager;
        private Mock<IDateTime> MockDateTime;
        private Mock<IGameUtil> MockGameUtil;
        private Mock<GameDriver> MockGameDriver;

        private GameUtil RealGameUtil;


        [SetUp]
        public void Init()
        {
            Container = new WindsorContainer();
            Container.Install(new GameTestInstaller());

            GameFacade = Container.Resolve<GameFacade>();
            ResearchFacade = Container.Resolve<ResearchFacade>();
            SupervisionFacade = Container.Resolve<SupervisionFacade>();
            TravelFacade = Container.Resolve<TravelFacade>();
            SpacemanFacade = Container.Resolve<SpacemanFacade>();
            PlanetFacade = Container.Resolve<PlanetFacade>();
            ActivityFacade = Container.Resolve<ActivityFacade>();
            ActivityTypeFacade = Container.Resolve<ActivityTypeFacade>();
            MessageFacade = Container.Resolve<MessageFacade>();

            MockManager = GameMockManager.Instance;

            MockDateTime = MockManager.MockDateTime;
            MockDateTime.Setup(d => d.Now).Returns(DateTime.Parse("2017-01-01T00:00:00"));

            MockGameUtil = MockManager.MockGameUtil;
            MockGameDriver = MockManager.MockGameDriver;

            MockGameDriver.Object.ActivityTypeFacade = Container.Resolve<ActivityTypeFacade>();
            MockGameDriver.Object.PlanetFacade = Container.Resolve<PlanetFacade>();

            MockGameDriver.Setup(d => d.GetConfig()).Returns(new GameTestConfig());

            RealGameUtil = new GameUtil();

            var contextFunc = Container.Resolve<Func<SpacemenTestDbContext>>();
            var context = contextFunc.Invoke();
            context.Nuke();
        }

        #endregion


        [Test]
        public async Task TestProceed_OnEmptyDb_PassesEntireScenario()
        {
            #region Creating Spacemen

            // possible error catching
            await GameFacade.Proceed();

            var s1 = new SpacemanBaseDto {Name = "Spaceman1"};
            await SpacemanFacade.Create(s1);
            Assert.AreNotEqual(Guid.Empty, s1.Id);
            Assert.AreNotEqual(Guid.Empty, s1.PlanetId);

            var s2 = new SpacemanBaseDto {Name = "Spaceman2"};
            await SpacemanFacade.Create(s2);
            Assert.AreNotEqual(Guid.Empty, s2.Id);
            // GameDriver should take care of searching for main planet id 
            // therefore would assign ID of existing planet (created alongside with s1)
            Assert.AreEqual(s1.PlanetId, s2.PlanetId);

            var s3 = new SpacemanBaseDto {Name = "Spaceman3"};
            await SpacemanFacade.Create(s3);

            #endregion

            #region Spaceman 1 Research scenario

            // Spaceman 1 Research scenario Setup:
            var activityDuration = MockGameDriver.Object.GetConfig().TimePerActivityInSeconds;
            var resRewardProb = MockGameDriver.Object.GetConfig().PercentageProbabilityOfResearchReward;
            var baseDate = DateTime.Parse("2017-01-01T00:00:00");
            const string newPlanetName = "Mars";

            // mock time
            MockDateTime.Setup(d => d.Now).Returns(baseDate);

            await ResearchFacade.StartActivity(s1.Id);

            // mock time edge value 
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration - 1));

            await GameFacade.Proceed();

            // mock Roll => true
            MockGameUtil.Setup(u => u.Roll(It.IsAny<int>())).Returns(true);
            // mock name generate =>
            MockGameUtil.Setup(u => u.GenerateRandomName()).Returns(newPlanetName);
            // we are on edge value => we will not get to the FinishActivity method
            MockGameUtil.Verify(u => u.Roll(resRewardProb), Times.Never);
            // mock time now allows FinishActivity
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration));

            await GameFacade.Proceed();

            // Spaceman 1 now finishes his Research and calls FinishActivity
            MockGameUtil.Verify(u => u.Roll(resRewardProb), Times.Once);

            // Assertions for this scenario
            s1 = await SpacemanFacade.Get(s1.Id);
            var a1 = await ActivityFacade.ListBySpacemanId(s1.Id);
            Assert.AreEqual(1, a1.Count());
            Assert.AreEqual(a1.First().SpacemanId, s1.Id);
            Assert.AreEqual(a1.First().Finished, true);
            var dp = await TravelFacade.ListDiscoveredPlanetsBySpacemanId(s1.Id);
            Assert.AreEqual(2, dp.Count());

            #endregion

            #region Spaceman 2 research and Spaceman 3 supervision

            var s2startingResearchRank = s2.ResearchRank;
            var s3startingInfluenceRank = s3.InfluenceRank;
            // mock start date
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration * 2));
            await ResearchFacade.StartActivity(s2.Id);
            await SupervisionFacade.StartActivity(s3.Id);
            // try proceed before end
            await GameFacade.Proceed();
            s2 = await SpacemanFacade.Get(s2.Id);
            s3 = await SpacemanFacade.Get(s3.Id);
            // Assertion for the proceed method call
            Assert.AreEqual(s2.ResearchRank, s2startingResearchRank);
            Assert.AreEqual(s3.InfluenceRank, s3startingInfluenceRank);
            // mock end date
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration * 3));
            await GameFacade.Proceed();

            // update entities
            s2 = await SpacemanFacade.Get(s2.Id);
            s3 = await SpacemanFacade.Get(s3.Id);
            // Assertions after the activities are finished
            Assert.AreEqual(MockGameDriver.Object.GetConfig().RankIncreaseForResearch + s2startingResearchRank,
                s2.ResearchRank);
            Assert.AreEqual(MockGameDriver.Object.GetConfig().RankIncreaseForSupervision + s3startingInfluenceRank,
                s3.InfluenceRank);

            #endregion

            #region Spaceman 1 travels to mars

            // update the object
            s1 = await SpacemanFacade.Get(s1.Id);
            var s1startingExplorationRank = s1.ExplorationRank;
            
            var planets = await PlanetFacade.All();
            var mars = planets.First(p => p.Name.Equals(newPlanetName));
            // mock start date
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration * 4));
            await TravelFacade.StartActivity(s1.Id, mars.Id);
            await GameFacade.Proceed();
            // assert that changes are not propagated before activity ends
            Assert.AreEqual(s1startingExplorationRank, s1.ExplorationRank);
            // mock end date
            MockDateTime.Setup(d => d.Now).Returns(baseDate.AddSeconds(activityDuration * 5));
            await GameFacade.Proceed();
            // update entity
            s1 = await SpacemanFacade.Get(s1.Id);
            // Assertions after the travel is finished
            Assert.AreEqual(mars.Id, s1.PlanetId);
            Assert.AreEqual(s1startingExplorationRank + MockGameDriver.Object.GetConfig().RankIncreaseForTravel, s1.ExplorationRank);

            #endregion

            await printAllTables();
        }

        #region Helper methods

        [Test]
        public async Task TestHelper()
        {
            await printAllPlanets();
        }

        #endregion

        #region Printing Methods

        private async Task printAllActivities()
            => printAll(await ActivityFacade.All(), "Activities");

        private async Task printAllActivityTypes()
            => printAll(await ActivityTypeFacade.All(), "ActivityTypes");

        private async Task printAllDiscoveredPlanets()
            => printAll(await TravelFacade.AllDiscoveredPlanets(), "DiscoveredPlanets");

        private async Task printAllMessages()
            => printAll(await MessageFacade.All(), "Messages");

        private async Task printAllPlanets()
            => printAll(await PlanetFacade.All(), "Planets");

        private async Task printAllSpacemen()
            => printAll(await SpacemanFacade.All(), "Spacemen");

        private async Task printAllSupervisedActivities()
            => printAll(await SupervisionFacade.AllSupervisedActivities(), "SupervisedActivities");

        private async Task printAllSupervisions()
            => printAll(await SupervisionFacade.All(), "Supervisions");

        private async Task printAllTravelActivities()
            => printAll(await TravelFacade.All(), "TravelActivities");
        //private async Task printAllEventLogs() => printAll(await EventLogFacade.All(), "EventLogs");

        private async Task printAllTables()
        {
            Console.WriteLine("\n##### DATABASE #####\n");
            await printAllPlanets();
            await printAllActivityTypes();
            await printAllSpacemen();
            await printAllActivities();
            await printAllDiscoveredPlanets();
            await printAllSupervisions();
            await printAllSupervisedActivities();
            await printAllSupervisedActivities();
            await printAllTravelActivities();
            await printAllMessages();
            //await printAllEventLogs();
        }

        private void printAll(IEnumerable<DtoBase> items, string title = "")
        {
            
            Console.Write($"\n\n#### {title}: ####");
            printAll(items);
//            if (items.IsNullOrEmpty())
//            {
//                Console.WriteLine($"#### /{title} ####");
//                return;
//            }
//            else
//            {
//                foreach (var item in items)
//                {
//                    Console.WriteLine($"{item}");
//                }
//            }
        }

        private void printAll(IEnumerable<DtoBase> items)
        {

            if (items.IsNullOrEmpty())
                return;
            var first = items.FirstOrDefault();
//            Console.Write($"####### {first.GetType().Name} #######");
            var props = first.GetType().GetProperties();
            var columns = props.Select(prop => prop.Name).ToList();
            var settings = new ConsoleTableSettings ('|', '-'); 
            
            var table = new ConsoleTable(columns, settings);

            foreach (var item in items)
            {
                var itemProps = item.GetType().GetProperties();
                var values = props.Select(prop => prop.GetValue(item, null)).ToList();
                var vals = values.Select(prop => prop.ToString()).ToList();
                table.AddRow(vals);
            }
            table.WriteToConsole();
        }

        #endregion
    }
}