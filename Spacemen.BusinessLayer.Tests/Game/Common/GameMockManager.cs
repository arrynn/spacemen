﻿using Moq;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;

namespace Spacemen.BusinessLayer.Tests.Game.Common
{
    internal class GameMockManager
    {
        private static GameMockManager _instance;

        public Mock<IDateTime> MockDateTime { get; private set; }
        public Mock<IGameUtil> MockGameUtil { get; private set; }
        public Mock<GameDriver> MockGameDriver { get; private set; }

        public static GameMockManager Instance => _instance ?? (_instance = new GameMockManager());

        private GameMockManager()
        {
            InitDateTimeMock();
            InitGameUtilMock();
            InitGameDriver();
        }

        private void InitGameUtilMock()
        {
            MockGameUtil = new Mock<IGameUtil>(MockBehavior.Strict);
        }

        private void InitDateTimeMock()
        {
            MockDateTime = new Mock<IDateTime>(MockBehavior.Strict);
        }
        
        private void InitGameDriver()
        {
            MockGameDriver = new Mock<GameDriver>(MockBehavior.Strict);
        }
        
    }
}