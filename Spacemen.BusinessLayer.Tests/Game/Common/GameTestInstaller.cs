﻿using AutoMapper;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Moq;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.Facades.Common;
using Spacemen.BusinessLayer.QueryObjects.Common;
using Spacemen.BusinessLayer.Services.Common;
using Spacemen.BusinessLayer.Utils.Date;
using Spacemen.BusinessLayer.Utils.Helpers;
using Spacemen.DataAccessLayer.config;

namespace Spacemen.BusinessLayer.Tests.Game.Common
{
    public class GameTestInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            new EntityFrameworkTestInstaller().Install(container, store);

            var mockManager = GameMockManager.Instance;
            
            
            container.Register(
                Classes.FromAssemblyNamed("Spacemen.BusinessLayer")
                    .BasedOn(typeof(QueryObjectBase<,,,>))
                    .WithServiceBase()
                    .LifestyleTransient(),
                Classes.FromAssemblyNamed("Spacemen.BusinessLayer")
                    .BasedOn<ServiceBase>()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient(),
                Classes.FromAssemblyNamed("Spacemen.BusinessLayer")
                    .BasedOn<FacadeBase>()
                    .LifestyleTransient(),
                Component.For<IMapper>()
                    .Instance(new Mapper(new MapperConfiguration(MappingConfig.ConfigureMapping)))
                    .LifestyleSingleton(),
                Component.For<IDateTime>()
                    .Instance(mockManager.MockDateTime.Object)
                    .LifestyleSingleton(),
//                Classes.FromAssemblyNamed("Spacemen.BusinessLayer")
//                    .BasedOn<IGameDriver>()
//                    .WithServiceBase()
//                    .LifestyleSingleton(),
                Component.For<IGameDriver>()
                    .Instance(mockManager.MockGameDriver.Object)
                    .LifestyleSingleton(),
                Component.For<IGameUtil>()
                    .Instance(mockManager.MockGameUtil.Object)
                    .LifestyleSingleton()
                
            );
        }
    }
}