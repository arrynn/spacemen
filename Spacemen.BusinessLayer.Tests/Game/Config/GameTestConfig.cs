﻿using Spacemen.BusinessLayer.Config;

namespace Spacemen.BusinessLayer.Tests.Game.Config
{
    public class GameTestConfig: IGameConfig
    {
        public int RankIncreaseForTravel => 3;
        public int PercentageTravelSpeedIncreasePerRank => 1;
        public int RankIncreaseForResearch => 3;
        public int RankIncreaseForSupervision => 1;
        public int RankIncreasePerSupervisedActivity => 1;
        public int PercentageProbabilityOfSupervisedActivityReward => 100;
        public int PercentageProbabilityOfResearchReward => 100;
        public int TimePerActivityInSeconds => 60;

        public string StartingPlanetName => "Earth";
        public string MainTravelActivityName => "Travel";
        public string MainResearchActivityName => "Research";
        public string MainSupervisionActivityName => "Supervision";
    }
}