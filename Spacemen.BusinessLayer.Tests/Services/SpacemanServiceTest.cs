﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Tests.Services.Common;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Tests.Services
{
    [TestFixture]
    class SpacemanServiceTest : ServiceTestBase<SpacemanService, Spaceman, SpacemanDto,SpacemanBaseDto, SpacemanFilterDto>
    {
        [Test]
        public async Task TestGetById_ReturnsCorrectResult()
        {
            const string name = "Michael";
            var id = Guid.NewGuid();
            var expectedDto = new SpacemanBaseDto {Name = name, Id = id};
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {Id = id},
                Items = new List<SpacemanBaseDto> {expectedDto},
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.GetByIdAsync(id);
                Assert.AreEqual(expectedDto, actualResult);
            }
        }
        
        [Test]
        public async Task TestGetById_OnEmptyQueryResult_ReturnsNull()
        {
            var id = Guid.NewGuid();
            var expectedId = Guid.NewGuid();
            object expectedDto = null;
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {Id = id},
                Items = new List<SpacemanBaseDto>(),
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.GetByIdAsync(expectedId);
                Assert.AreEqual(expectedDto, actualResult);
            }
        }

        [Test]
        public async Task TestGetByName_ReturnsCorrectResult()
        {
            const string name = "Michael";
            var expectedDto = new SpacemanBaseDto {Name = name};
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {Name = name},
                Items = new List<SpacemanBaseDto> {expectedDto},
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.GetByNameAsync(name);
                Assert.AreEqual(expectedDto, actualResult);
            }
        }
        
        [Test]
        public async Task TestGetByName_OnEmptyQueryResult_ReturnsNull()
        {
            const string name = "Michael";
            object expectedDto = null;
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {Name = name},
                Items = new List<SpacemanBaseDto>(),
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.GetByNameAsync(name);
                Assert.AreEqual(expectedDto, actualResult);
            }
        }
        
        [Test]
        public async Task TestListAsync_ReturnsCorrectResult()
        {
            
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {},
                Items = new List<SpacemanBaseDto>
                {
                    new SpacemanBaseDto {Name = "Michael", Id = Guid.NewGuid()},
                    new SpacemanBaseDto {Name = "Arnold", Id = Guid.NewGuid()},
                },
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.ListAsync(expectedQueryResult.Filter);
                Assert.AreEqual(expectedQueryResult, actualResult);
            }
        }
        
        [Test]
        public async Task TestListAsync_OnEmptyQueryResult_ReturnsCorrectResult()
        {
            
            var expectedQueryResult = new QueryResultDto<SpacemanBaseDto, SpacemanFilterDto>
            {
                Filter = new SpacemanFilterDto {Name = "Gross"},
                Items = new List<SpacemanBaseDto>()
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.ListAsync(expectedQueryResult.Filter);
                Assert.AreEqual(expectedQueryResult, actualResult);
            }
        }
        
        
    }
}