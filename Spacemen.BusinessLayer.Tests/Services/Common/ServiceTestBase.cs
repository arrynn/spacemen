﻿using System;
using AutoMapper;
using Castle.Windsor;
using Spacemen.BusinessLayer.Config;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.Infrastructure;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Tests.Services.Common
{
    public abstract class ServiceTestBase<TService, TEntity, TDto, TBaseDto, TFilterDto>
        where TService : class
        where TEntity : class, IEntity, new()
        where TDto : DtoBase, TBaseDto
        where TBaseDto : DtoBase
        where TFilterDto : FilterDtoBase, new()
    {
        protected readonly IUnitOfWorkProvider Provider;
        protected readonly IMapper Mapper;

        protected ServiceTestBase()
        {
            var container = new WindsorContainer();
            container.Install(new BusinessLayerInstaller());

            Provider = ServiceMockManager.ConfigureUowMock().Object;
            Mapper = ServiceMockManager.ConfigureRealMapper();
        }

        protected static TService CreateService(QueryResultDto<TBaseDto, TFilterDto> expectedQueryResult)
        {
            var mockManager = new ServiceMockManager();
            var mapper = ServiceMockManager.ConfigureRealMapper();
            var repositoryMock = mockManager.ConfigureRepositoryMock<TEntity>();
            var queryMock = mockManager.ConfigureQueryObjectMock<TBaseDto, TEntity, TFilterDto>(expectedQueryResult);
            var service =
                (TService) Activator.CreateInstance(typeof(TService), mapper, repositoryMock.Object, queryMock.Object);
            return service;
        }
    }
}