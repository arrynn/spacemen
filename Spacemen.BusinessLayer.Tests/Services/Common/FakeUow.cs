﻿using System.Threading.Tasks;
using Spacemen.Infrastructure.UnitOfWork;

namespace Spacemen.BusinessLayer.Tests.Services.Common
{
    internal class FakeUow : UnitOfWorkBase
    {
        protected override Task CommitCore()
        {
            return Task.Delay(15);
        }

        public override void Dispose()
        {
        }
    }
}