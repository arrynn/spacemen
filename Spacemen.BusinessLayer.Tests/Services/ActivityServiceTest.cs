﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spacemen.BusinessLayer.DataTransferObjects;
using Spacemen.BusinessLayer.DataTransferObjects.Common;
using Spacemen.BusinessLayer.DataTransferObjects.Filters;
using Spacemen.BusinessLayer.Services.Activities;
using Spacemen.BusinessLayer.Services.Spacemen;
using Spacemen.BusinessLayer.Tests.Services.Common;
using Spacemen.DataAccessLayer.Entities;

namespace Spacemen.BusinessLayer.Tests.Services
{
    [TestFixture]
    class ActivityServiceTest : ServiceTestBase<ActivityService, Activity, ActivityDto, ActivityBaseDto, ActivityFilterDto>
    {
        [Test]
        public async Task TestGetById_ReturnsCorrectResult()
        {
            var id = Guid.NewGuid();
            var expectedDto = new ActivityBaseDto()
            {
                Start = DateTime.Now,
                End = DateTime.Now.AddHours(1),
                ActivityTypeId = Guid.NewGuid(),
                SpacemanId = Guid.NewGuid()
            };
            var expectedQueryResult = new QueryResultDto<ActivityBaseDto, ActivityFilterDto>
            {
                Filter = new ActivityFilterDto{Id = id},
                Items = new List<ActivityBaseDto> {expectedDto},
            };

            var service = CreateService(expectedQueryResult);

            using (Provider.Create())
            {
                var actualResult = await service.GetByIdAsync(id);
                Assert.AreEqual(expectedDto, actualResult);
            }
        }

        [Test]
        public async Task TestUpdate_ReturnsCorrectResult()
        {
            var id = Guid.NewGuid();
            var expectedDto = new ActivityBaseDto()
            {
                Start = DateTime.Now,
                End = DateTime.Now.AddHours(1),
                ActivityTypeId = Guid.NewGuid(),
                SpacemanId = Guid.NewGuid()
            };
            var expectedQueryResult = new QueryResultDto<ActivityBaseDto, ActivityFilterDto>
            {
                Filter = new ActivityFilterDto{Id = id},
                Items = new List<ActivityBaseDto> {expectedDto},
            };

            var service = CreateService(expectedQueryResult);

            using (var uow = Provider.Create())
            {
                var actualResult = await service.GetByIdAsync(id);
                Assert.AreEqual(expectedDto, actualResult);

                actualResult.Finished = true;

                await service.Update(actualResult);
                await uow.Commit();
            }
        }

    }
}